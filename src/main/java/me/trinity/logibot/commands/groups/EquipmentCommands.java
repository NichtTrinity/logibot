package me.trinity.logibot.commands.groups;

import me.trinity.logibot.LogiBot;
import me.trinity.logibot.commands.Command;
import me.trinity.logibot.commands.CommandGroup;
import me.trinity.logibot.data.equipment.Equipment;
import me.trinity.logibot.data.equipment.SpecialOrder;
import me.trinity.logibot.utils.SubmitRequest;
import org.javacord.api.entity.message.MessageFlag;
import org.javacord.api.entity.message.component.ActionRow;
import org.javacord.api.entity.message.component.TextInput;
import org.javacord.api.entity.message.component.TextInputStyle;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.user.User;
import org.javacord.api.interaction.SlashCommandOption;
import org.javacord.api.interaction.SlashCommandOptionChoice;
import org.javacord.api.interaction.SlashCommandOptionType;
import org.javacord.api.interaction.callback.InteractionImmediateResponseBuilder;

import java.awt.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

public class EquipmentCommands extends CommandGroup {
    @Override
    public void registerCommands() {
        super.addCommands(Command.<Void>builder("removeequipment", "Removes an equipment from a stockpile")
                        .options(
                                SlashCommandOption.createStringOption("name", "Name of the stockpile", true, true),
                                SlashCommandOption.createStringOption("equipmentType", "Name of the equipment", true, true)
                        ).restricted(true)
                        .function(interaction -> CompletableFuture.runAsync(() -> {
                            try {
                                String name = interaction.getArgumentStringValueByName("stockpile").get();
                                Equipment.Type equipmentType = Equipment.Type.valueOf(interaction.getArgumentStringValueByName("equipmentType").get());
                                interaction.getServer().ifPresent(server -> {
                                    if (LogiBot.getServerStockpiles().stream().noneMatch(stockpile -> stockpile.getServerId() == server.getId())) {
                                        interaction.createImmediateResponder().setContent("This server does not have any stockpiles").setFlags(MessageFlag.EPHEMERAL).respond();
                                        return;
                                    }
                                    LogiBot.getServerStockpiles().stream().filter(stockpile -> stockpile.getName().equalsIgnoreCase(name) && stockpile.getServerId() == server.getId()).findFirst().ifPresentOrElse(stockpile -> {
                                        if (stockpile.getContents().stream().noneMatch(equipment -> equipment.getType().equals(equipmentType))) {
                                            interaction.createImmediateResponder().setContent("This equipment has not been added to the stockpile.").setFlags(MessageFlag.EPHEMERAL).respond();
                                            return;
                                        }
                                        stockpile.removeEquipment(equipmentType);
                                        EmbedBuilder embedBuilder = new EmbedBuilder();
                                        embedBuilder.setTitle(equipmentType.getUserFriendlyName())
                                                .setDescription("*Removed*")
                                                .setColor(new Color(255, 48, 48));
                                        interaction.createImmediateResponder().addEmbed(embedBuilder).setFlags(MessageFlag.EPHEMERAL).respond();
                                        stockpile.save();
                                    }, () -> interaction.createImmediateResponder().setContent("Stockpile with the name '" + name + "' was not found.").setFlags(MessageFlag.EPHEMERAL).respond());
                                });
                            } catch (IllegalArgumentException e) {
                                interaction.createImmediateResponder().setContent("The equipment was not found. (Check spelling)").setFlags(MessageFlag.EPHEMERAL).respond();
                            }
                        })).build(),
                Command.<Void>builder("addequipment", "Adds an equipment to a stockpile")
                        .options(
                                SlashCommandOption.createStringOption("name", "Name of the stockpile", true, true),
                                SlashCommandOption.createStringOption("equipmentType", "Name of the equipment", true, true),
                                SlashCommandOption.createDecimalOption("desiredAmount", "The amount (of crates) that should be kept in stock", true),
                                SlashCommandOption.createBooleanOption("isPriority", "Whether this equipment is considered a priority", false),
                                SlashCommandOption.createBooleanOption("isIndividual", "Whether this equipment represents a individual item or a crate", false)
                        ).restricted(true)
                        .function(interaction -> CompletableFuture.runAsync(() -> {
                            try {
                                String name = interaction.getArgumentStringValueByName("name").get();
                                Equipment.Type equipmentType = Equipment.Type.valueOf(interaction.getArgumentStringValueByName("equipmentType").get()); // I get the EquipmentType directly from the String since the user gets tab completions for them.
                                int amountRequired = interaction.getArgumentDecimalValueByName("desiredAmount").get().intValue(); // Let's not use long when the highest number is 300, okay?
                                boolean priority = interaction.getArgumentBooleanValueByName("isPriority").orElse(false);
                                boolean individual = interaction.getArgumentBooleanValueByName("isIndividual").orElse(false);

                                interaction.getServer().ifPresent(server -> {
                                    if (LogiBot.getServerStockpiles().stream().noneMatch(stockpile -> stockpile.getServerId() == server.getId())) {
                                        interaction.createImmediateResponder().setContent("This server does not have any stockpiles").setFlags(MessageFlag.EPHEMERAL).respond();
                                        return;
                                    }
                                    LogiBot.getServerStockpiles().stream().filter(stockpile -> stockpile.getName().equalsIgnoreCase(name) && stockpile.getServerId() == server.getId()).findFirst().ifPresentOrElse(stockpile -> {
                                        if (stockpile.getContents().stream().anyMatch(equipment -> equipment.getType().equals(equipmentType))) {
                                            interaction.createImmediateResponder().setContent("This equipment has already been added to the stockpile.").setFlags(MessageFlag.EPHEMERAL).respond();
                                            return;
                                        }
                                        if (individual && equipmentType.getCategory() != Equipment.Category.SHIPPABLES && equipmentType.getCategory() != Equipment.Category.VEHICLES) {
                                            interaction.createImmediateResponder().setContent("Individual items in stockpiles are only supported for Vehicles and Shippables!").setFlags(MessageFlag.EPHEMERAL).respond();
                                            return;
                                        }
                                        Equipment equipment = new Equipment(equipmentType, 0, amountRequired, priority, individual);
                                        stockpile.addEquipment(equipment);
                                        EmbedBuilder embedBuilder = new EmbedBuilder();
                                        embedBuilder.setTitle(equipmentType.getUserFriendlyName())
                                                .addField("Amount present", String.valueOf(equipment.getAmountPresent()))
                                                .addField("Amount requested", String.valueOf(equipment.getAmountRequested()))
                                                .addField("Priority", String.valueOf(equipment.isPriority()))
                                                .setColor(new Color(255, 144, 48));
                                        interaction.createImmediateResponder().addEmbed(embedBuilder).setFlags(MessageFlag.EPHEMERAL).respond();
                                        stockpile.save();
                                    }, () -> interaction.createImmediateResponder().setContent("Stockpile with the name '" + name + "' was not found.").setFlags(MessageFlag.EPHEMERAL).respond());
                                });
                            } catch (IllegalArgumentException e) { // In case they fuck it up somehow
                                interaction.createImmediateResponder().setContent("The equipment was not found. (Check spelling)").setFlags(MessageFlag.EPHEMERAL).respond();
                            }
                        })).build(),
                Command.<Void>builder("editequipment", "Adjust the requested amount and priority of an equipment")
                        .options(
                                SlashCommandOption.createStringOption("name", "Name of the stockpile", true, true),
                                SlashCommandOption.createStringOption("equipmentType", "Name of the equipment", true, true),
                                SlashCommandOption.createDecimalOption("desiredAmount", "The amount (of crates) that should be kept in stock", true),
                                SlashCommandOption.createBooleanOption("isPriority", "Whether this equipment is considered a priority", false)
                        ).restricted(true)
                        .function(interaction -> CompletableFuture.runAsync(() -> {
                            String name = interaction.getArgumentStringValueByName("stockpile").get();
                            Equipment.Type equipmentType = Equipment.Type.valueOf(interaction.getArgumentStringValueByName("equipmentType").get());
                            int amountRequired = interaction.getArgumentDecimalValueByName("desiredAmount").get().intValue();
                            Optional<Boolean> priority = interaction.getArgumentBooleanValueByName("isPriority");

                            interaction.getServer().ifPresent(server -> {
                                if (LogiBot.getServerStockpiles().stream().noneMatch(stockpile -> stockpile.getServerId() == server.getId())) {
                                    interaction.createImmediateResponder().setContent("This server does not have any stockpiles").setFlags(MessageFlag.EPHEMERAL).respond();
                                    return;
                                }
                                LogiBot.getServerStockpiles().stream().filter(stockpile -> stockpile.getName().equalsIgnoreCase(name) && stockpile.getServerId() == server.getId()).findFirst().ifPresentOrElse(stockpile -> {
                                    stockpile.getContents().stream().filter(equipment -> equipment.getType().equals(equipmentType)).findFirst().ifPresentOrElse(equipment -> {
                                        equipment.setAmountRequested(amountRequired);
                                        priority.ifPresent(equipment::setPriority);
                                        interaction.createImmediateResponder().addEmbed(new EmbedBuilder().setTitle(equipmentType.getUserFriendlyName())
                                                .addField("Amount present", String.valueOf(equipment.getAmountPresent()))
                                                .addField("Amount requested", String.valueOf(equipment.getAmountRequested()))
                                                .addField("Priority", String.valueOf(equipment.isPriority()))
                                                .setColor(new Color(255, 144, 48))).setFlags(MessageFlag.EPHEMERAL).respond();
                                        stockpile.save();
                                    }, () -> interaction.createImmediateResponder().setContent("This equipment has not been added to the stockpile.").setFlags(MessageFlag.EPHEMERAL).respond());
                                }, () -> interaction.createImmediateResponder().setContent("Stockpile with the name '" + name + "' was not found.").setFlags(MessageFlag.EPHEMERAL).respond());
                            });
                        })).build(),
                Command.<Void>builder("submitequipment", "Submit multiple different equipment at once")
                        .options(SlashCommandOption.createStringOption("name", "Name of the stockpile", true, true))
                        .function(interaction -> CompletableFuture.runAsync(() -> {
                            String name = interaction.getArgumentStringValueByName("name").get();
                            interaction.getServer().ifPresent(server -> {
                                // This checks if user has already made a submit request in the same server without finishing it (confirming/cancelling). 1 active submit per user per server max.
                                if (LogiBot.getViews().stream().filter(userView -> userView instanceof SubmitRequest).map(userView -> (SubmitRequest) userView).anyMatch(request -> request.getServer().getId() == server.getId() && request.getUser().getId() == interaction.getUser().getId())) {
                                    interaction.createImmediateResponder().setContent("You have a pending submit to evaluate, please confirm or cancel it before making another one!").setFlags(MessageFlag.EPHEMERAL).respond();
                                    return;
                                }
                                if (LogiBot.getServerStockpiles().stream().noneMatch(stockpile -> stockpile.getServerId() == server.getId())) {
                                    interaction.createImmediateResponder().setContent("This server does not have any stockpiles").setFlags(MessageFlag.EPHEMERAL).respond();
                                    return;
                                }
                                LogiBot.getServerStockpiles().stream().filter(stockpile -> stockpile.getName().equalsIgnoreCase(name) && stockpile.getServerId() == server.getId()).findFirst().ifPresentOrElse(stockpile -> {
                                    interaction.respondWithModal("bulkmodal", "Submit your equipment in bulk!",
                                            ActionRow.of(TextInput.create(TextInputStyle.PARAGRAPH, "bulkmodal_input",
                                                    "Submit to \"" + stockpile.getName() + "\" :",
                                                    "Input format: <Equipment Name> <Amount>\nCatara mo.II 1\nbmats 5\nHauler 1c\n...", "", true)));
                                    SubmitRequest submitRequest = new SubmitRequest(interaction.getUser(), server, stockpile);
                                    LogiBot.getViews().add(submitRequest);
                                }, () -> interaction.createImmediateResponder().setContent("Stockpile with the name '" + name + "' was not found.").setFlags(MessageFlag.EPHEMERAL).respond());
                            });
                        })).build(),
                Command.<Void>builder("specialorder", "-")
                        .options(
                                SlashCommandOption.createSubcommand("add", "Add a new special order", Arrays.asList(
                                                SlashCommandOption.createStringOption("equipmentType", "The name of the equipment", true, true),
                                                SlashCommandOption.createDecimalOption("desiredAmount", "The amount that is needed", true),
                                                SlashCommandOption.createWithChoices(SlashCommandOptionType.STRING, "priority", "The priority level of the special order", true,
                                                        Arrays.stream(SpecialOrder.Priority.values()).map(priority -> SlashCommandOptionChoice.create(priority.toString(), priority.toString())).toList()),
                                                SlashCommandOption.createStringOption("message", "Optional to be displayed", false)
                                        )
                                ),
                                SlashCommandOption.createSubcommand("delete", "Delete a special order", Collections.singletonList(
                                        SlashCommandOption.createStringOption("id", "The id of the special order", true, true)
                                )),
                                SlashCommandOption.createSubcommand("edit", "Edit a special order", Arrays.asList(
                                        SlashCommandOption.createStringOption("id", "The id of the special order", true, true),
                                        SlashCommandOption.createDecimalOption("desiredAmount", "The amount that is needed", true),
                                        SlashCommandOption.createWithChoices(SlashCommandOptionType.STRING, "priority", "The priority level of the special order", true,
                                                Arrays.stream(SpecialOrder.Priority.values()).map(priority -> SlashCommandOptionChoice.create(priority.toString(), priority.toString())).toList()),
                                        SlashCommandOption.createStringOption("message", "Optional to be displayed", false)
                                )),
                                SlashCommandOption.createSubcommand("complete", "Mark a special order as completed", Collections.singletonList(
                                        SlashCommandOption.createStringOption("id", "The id of the special order", true, true)
                                )),
                                SlashCommandOption.createSubcommand("list", "Display all current special orders")
                        ).restricted(true)
                        .function(interaction -> CompletableFuture.runAsync(() -> {
                            User user = interaction.getUser();
                            interaction.getServer().ifPresent(server -> {
                                switch (interaction.getOptions().get(0).getName()) {
                                    case "add" -> {
                                        try {
                                            Equipment.Type type = Equipment.Type.valueOf(interaction.getArgumentStringValueByName("equipmentType").get());
                                            int amount = interaction.getArgumentDecimalValueByName("desiredAmount").get().intValue();
                                            SpecialOrder.Priority priority = SpecialOrder.Priority.valueOf(interaction.getArgumentStringValueByName("priority").get());
                                            Optional<String> message = interaction.getArgumentStringValueByName("message");

                                            SpecialOrder specialOrder = new SpecialOrder(new Equipment(type, 0, amount, false, false), message.orElse(""), priority, server.getId(), user.getId());
                                            LogiBot.getSpecialOrders().add(specialOrder);
                                            interaction.createImmediateResponder().addEmbed(specialOrder.constructEmbed()).respond();
                                        } catch (IllegalArgumentException e) {
                                            interaction.createImmediateResponder().setContent("Unknown argument. (Check spelling)").setFlags(MessageFlag.EPHEMERAL).respond();
                                        }
                                    }
                                    case "delete" -> {
                                        String id = interaction.getArgumentStringValueByName("id").get();
                                        LogiBot.getSpecialOrders().stream().filter(specialOrder -> specialOrder.getId().equals(id)).findFirst().ifPresentOrElse(specialOrder -> {
                                            specialOrder.delete();
                                            interaction.createImmediateResponder().setContent("Special Order with ID '" + id + "' deleted.").setFlags(MessageFlag.EPHEMERAL).respond();
                                        }, () -> interaction.createImmediateResponder().setContent("Special Order with ID '" + id + "' not found."));
                                    }
                                    case "edit" -> {
                                        try {
                                            String id = interaction.getArgumentStringValueByName("id").get();
                                            int amount = interaction.getArgumentDecimalValueByName("desiredAmount").get().intValue();
                                            SpecialOrder.Priority priority = SpecialOrder.Priority.valueOf(interaction.getArgumentStringValueByName("priority").get());
                                            Optional<String> message = interaction.getArgumentStringValueByName("message");
                                            LogiBot.getSpecialOrders().stream().filter(specialOrder -> specialOrder.getId().equals(id)).findFirst().ifPresentOrElse(specialOrder -> {
                                                specialOrder.getEquipment().setAmountRequested(amount);
                                                specialOrder.setPriority(priority);
                                                message.ifPresent(specialOrder::setMessage);
                                                interaction.createImmediateResponder().setContent("Special Order with ID '" + id + "' successfully edit.");
                                            }, () -> interaction.createImmediateResponder().setContent("Special Order with ID '" + id + "' not found."));
                                        } catch (IllegalArgumentException e) {
                                            interaction.createImmediateResponder().setContent("Unknown argument. (Check spelling)").setFlags(MessageFlag.EPHEMERAL).respond();
                                        }
                                    }
                                    case "complete" -> {
                                        String id = interaction.getArgumentStringValueByName("id").get();
                                        LogiBot.getSpecialOrders().stream().filter(specialOrder -> specialOrder.getId().equals(id)).findFirst().ifPresentOrElse(specialOrder -> {
                                            interaction.createImmediateResponder().addEmbed(specialOrder.constructCompleteEmbed()).respond();
                                            specialOrder.delete();
                                        }, () -> interaction.createImmediateResponder().setContent("Special Order with ID '" + id + "' not found."));
                                    }
                                    case "list" -> {
                                        List<SpecialOrder> specialOrders = LogiBot.getSpecialOrders();
                                        if (specialOrders.size() == 0) {
                                            interaction.createImmediateResponder().setContent("Currently there are no special orders.").setFlags(MessageFlag.EPHEMERAL).respond();
                                            return;
                                        }
                                        InteractionImmediateResponseBuilder responseBuilder = interaction.createImmediateResponder();
                                        specialOrders.forEach(specialOrder -> {
                                            responseBuilder.addEmbed(specialOrder.constructEmbed()); // Might fail if we have too many characters or embeds
                                        });
                                        responseBuilder.respond();
                                    }
                                }
                            });
                        })).build()
        );
    }
}
