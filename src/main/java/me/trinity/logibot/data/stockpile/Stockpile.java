package me.trinity.logibot.data.stockpile;

import com.google.common.collect.Lists;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.ReplaceOptions;
import me.trinity.logibot.LogiBot;
import me.trinity.logibot.data.equipment.Equipment;
import me.trinity.logibot.utils.DatabaseSerializable;
import me.trinity.logibot.utils.ProgressBar;
import org.bson.Document;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.user.User;

import java.awt.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

public class Stockpile implements DatabaseSerializable {

    public Stockpile(long serverId, String name, User creator, String location, String password) {
        this(serverId, name, creator.getId(), location, password);
    }

    public Stockpile(long serverId, String name, long creatorId, String location, String password) {
        this(serverId, name, creatorId, location, password, new ArrayList<>());
    }

    public Stockpile(long serverId, String name, long creatorId, String location, String password, List<Equipment> contents) {
        this.serverId = serverId;
        this.name = name;
        this.location = location;
        this.password = password;
        this.creatorId = creatorId;
        this.contents = new ArrayList<>(contents);
    }

    private final long serverId;
    private final String name;
    private final String location;
    private final String password;
    private final long creatorId;
    private final ArrayList<Equipment> contents;

    public long getServerId() {
        return serverId;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public String getPassword() {
        return password;
    }

    public long getCreatorId() {
        return creatorId;
    }

    public ArrayList<Equipment> getContents() {
        return contents;
    }

    public void addEquipment(Equipment equipment) {
        if (this.contents.contains(equipment) || this.contents.stream().anyMatch(e -> e.getType().equals(equipment.getType())))
            return;
        this.contents.add(equipment);
    }

    public void removeEquipment(Equipment equipment) {
        this.contents.remove(equipment);
    }

    public void removeEquipment(Equipment.Type equipmentType) {
        this.contents.removeIf(equipment -> equipment.getType().equals(equipmentType));
    }

    public Optional<Equipment> getEquipment(Equipment.Type equipmentType) {
        return this.contents.stream().filter(equipment -> equipment.getType().equals(equipmentType)).findFirst();
    }

    public void submitEquipment(Equipment.Type type, Integer amount, boolean individual) {
        this.contents.stream().filter(equipment -> equipment.getType().equals(type) && equipment.isIndividual() == individual).findFirst()
                .ifPresentOrElse(equipment -> equipment.setAmountPresent(equipment.getAmountPresent() + amount),
                        () -> this.contents.add(new Equipment(type, amount, 0, false, individual)));
    }


    public List<EmbedBuilder> constructCategoryEmbed(Equipment.Category category, boolean full) {
        ArrayList<EmbedBuilder> embeds = new ArrayList<>();
        EmbedBuilder embedBuilder;
        StringBuilder stringBuilder = new StringBuilder();
        String crateIndicator = " (Crate)";
        List<Equipment> list;
        int maxLength;
        String whitespace = " ";
        if (!full) {
            list = this.contents.stream().filter(equipment -> equipment.getType().getCategory().equals(category) && equipment.getAmountRequested() != 0).toList();
            if (list.size() != 0) {
                for (List<Equipment> partition : Lists.partition(list, 22)) {
                    embedBuilder = new EmbedBuilder()
                            .setTitle("Stockpile: " + this.name)
                            .setDescription("***" + category.getUserFriendlyName() + "***")
                            .setColor(new Color(83, 49, 255));
                    if (category == Equipment.Category.SHIPPABLES || category == Equipment.Category.VEHICLES) {
                        maxLength = partition.stream().map(equipment -> equipment.getType().getUserFriendlyName().length()).max(Comparator.comparingInt(value -> value)).get() + crateIndicator.length();
                    } else {
                        maxLength = partition.stream().map(equipment -> equipment.getType().getUserFriendlyName().length()).max(Comparator.comparingInt(value -> value)).get();
                    }
                    for (Equipment equipment : partition) {
                        String name = "`" + equipment.getType().getUserFriendlyName() + whitespace.repeat(maxLength - equipment.getType().getUserFriendlyName().length()) + "`";
                        if (category == Equipment.Category.VEHICLES || category == Equipment.Category.SHIPPABLES && !equipment.isIndividual()) {
                            name = "`" + equipment.getType().getUserFriendlyName() + crateIndicator + whitespace.repeat(maxLength - equipment.getType().getUserFriendlyName().length() - crateIndicator.length()) + "`";
                        }
                        String count = equipment.getAmountPresent() + "/" + equipment.getAmountRequested();
                        count = "`" + whitespace.repeat(7 - count.length()) + count;
                        stringBuilder.append(name)
                                .append(equipment.calculateAmountNeeded() <= 0 ? ProgressBar.create(10, ProgressBar.Color.random()) : ProgressBar.create((int) Math.floor(((float) equipment.getAmountPresent() / (float) equipment.getAmountRequested()) * 10), ProgressBar.Color.random()))
                                .append(count);
                        if (equipment.getAmountPresent() < equipment.getAmountRequested()) {
                            String needed = equipment.calculateAmountNeeded() + " needed";
                            needed = whitespace.repeat(10 - needed.length()) + needed;
                            stringBuilder.append(" | ").append(needed).append("`");
                            if (equipment.isPriority()) stringBuilder.append("<a:priority:1120089284125413487>");
                        } else {
                            String done = "DONE ✓";
                            String s = whitespace.repeat((10 - done.length()) / 2);
                            done = s + done + s;
                            stringBuilder.append(" | ")
                                    .append(done)
                                    .append("`");
                        }
                        embedBuilder.addField("", stringBuilder.toString());
                        stringBuilder.setLength(0);
                    }
                    embeds.add(embedBuilder);
                }
            }
        } else {
            list = this.contents.stream().filter(equipment -> equipment.getType().getCategory().equals(category)).toList();
            if (list.size() != 0) {
                for (List<Equipment> partition : Lists.partition(list, 22)) {
                    embedBuilder = new EmbedBuilder()
                            .setTitle("Stockpile: " + this.name)
                            .setDescription("***" + category.getUserFriendlyName() + "***")
                            .setColor(new Color(83, 49, 255));
                    if (category == Equipment.Category.SHIPPABLES || category == Equipment.Category.VEHICLES) {
                        maxLength = partition.stream().map(equipment -> equipment.getType().getUserFriendlyName().length()).max(Comparator.comparingInt(value -> value)).get() + crateIndicator.length();
                    } else {
                        maxLength = partition.stream().map(equipment -> equipment.getType().getUserFriendlyName().length()).max(Comparator.comparingInt(value -> value)).get();
                    }
                    for (Equipment equipment : partition) {
                        String name = "`" + equipment.getType().getUserFriendlyName() + whitespace.repeat(maxLength - equipment.getType().getUserFriendlyName().length()) + "`";
                        if (category == Equipment.Category.VEHICLES && !equipment.isIndividual() || category == Equipment.Category.SHIPPABLES && !equipment.isIndividual()) {
                            name = "`" + equipment.getType().getUserFriendlyName() + crateIndicator + whitespace.repeat(maxLength - equipment.getType().getUserFriendlyName().length() - crateIndicator.length()) + "`";
                        }
                        String count = equipment.getAmountPresent() + "/" + equipment.getAmountRequested();
                        count = "`" + whitespace.repeat(7 - count.length()) + count;
                        stringBuilder.append(name)
                                .append(equipment.getAmountPresent() == 0 ? ProgressBar.create(0, ProgressBar.Color.WHITE) : ProgressBar.create((int) Math.floor(((float) equipment.getAmountPresent() / (float) equipment.getType().getStorageType().getMaxStorageAmount()) * 10), ProgressBar.Color.random()))
                                .append(count).append("`\n");
                        embedBuilder.addField("", stringBuilder.toString());
                        stringBuilder.setLength(0);
                    }
                    embeds.add(embedBuilder);
                }
            }
        }
        return embeds;
    }

    public List<EmbedBuilder> constructAllCategoriesEmbeds(boolean full) {
        ArrayList<EmbedBuilder> list = new ArrayList<>();
        for (Equipment.Category category : Equipment.Category.values()) {
            List<EmbedBuilder> embeds = this.constructCategoryEmbed(category, full);
            if (embeds.size() != 0) list.addAll(embeds);
        }
        return list;
    }

    @Override
    public Document toDocument() {
        return new Document("serverId", this.serverId)
                .append("name", this.name)
                .append("location", location)
                .append("password", password)
                .append("creatorId", this.creatorId)
                .append("contents", this.contents.stream().map(Equipment::toDocument).toList());
    }

    @Override
    public void save() {
        CompletableFuture.runAsync(() -> {
            try (MongoClient client = LogiBot.getDatabaseService().connect()) {
                MongoDatabase database = client.getDatabase("logibot");
                MongoCollection<Document> collection = database.getCollection("stockpiles");
                collection.replaceOne(Filters.and(Filters.eq("serverId", this.serverId), Filters.eq("name", this.name)),
                        this.toDocument(), new ReplaceOptions().upsert(true));
            }
        });
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Stockpile s) {
            return s.getName().equals(this.name);
        }
        return false;
    }
}
