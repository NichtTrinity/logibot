package me.trinity.logibot.utils;

import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;

public interface UserView {

    User getUser();

    Server getServer();

    void invalidate();

}
