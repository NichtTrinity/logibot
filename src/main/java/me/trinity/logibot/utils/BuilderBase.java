package me.trinity.logibot.utils;

public interface BuilderBase<T> {

    T build();

}
