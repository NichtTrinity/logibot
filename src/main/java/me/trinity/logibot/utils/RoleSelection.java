package me.trinity.logibot.utils;

import me.trinity.logibot.LogiBot;
import org.javacord.api.entity.permission.Role;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class RoleSelection implements UserView {

    public RoleSelection(User user, Server server) {
        this.user = user;
        this.server = server;
        this.roles = new HashMap<>();
        this.messageIds = new ArrayList<>();
    }

    private final User user;
    private final Server server;
    private final HashMap<Integer, List<Role>> roles;
    private final ArrayList<Long> messageIds;

    public HashMap<Integer, List<Role>> getRoles() {
        return roles;
    }

    public ArrayList<Long> getMessageIds() {
        return messageIds;
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public Server getServer() {
        return server;
    }

    @Override
    public void invalidate() {
        LogiBot.getExecutorService().schedule(() -> LogiBot.getViews().remove(this), 10, TimeUnit.MINUTES);
    }
}
