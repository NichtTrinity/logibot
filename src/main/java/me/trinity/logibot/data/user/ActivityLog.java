package me.trinity.logibot.data.user;

import com.google.common.collect.Lists;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.ReplaceOptions;
import lombok.Getter;
import me.trinity.logibot.LogiBot;
import me.trinity.logibot.data.equipment.Equipment;
import me.trinity.logibot.utils.DatabaseSerializable;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.bson.Document;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;

import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Getter
public class ActivityLog implements DatabaseSerializable {

    private final long serverId;
    private final long userId;
    private final HashMap<Equipment.Type, MutablePair<Integer, Boolean>> totalSubmits;
    private int totalSupplyValue;

    public ActivityLog(User user, Server server) {
        this(user.getId(), server.getId());
    }

    public ActivityLog(long userId, long serverId) {
        this(userId, serverId, new HashMap<>());
    }

    public ActivityLog(User user, Server server, Map<Equipment.Type, MutablePair<Integer, Boolean>> totalSubmits) {
        this(user.getId(), server.getId(), totalSubmits);
    }

    public ActivityLog(long userId, long serverId, Map<Equipment.Type, MutablePair<Integer, Boolean>> totalSubmits) {
        this.userId = userId;
        this.serverId = serverId;
        this.totalSubmits = new HashMap<>(totalSubmits);
        this.calculateTotalSupplyValue();
    }

    public static CompletableFuture<Optional<ActivityLog>> load(User user, Server server) {
        return load(user.getId(), server.getId());
    }

    public static CompletableFuture<Optional<ActivityLog>> load(long userId, long serverId) {
        return CompletableFuture.supplyAsync(() -> {
            try (MongoClient client = LogiBot.getDatabaseService().connect()) {
                MongoDatabase database = client.getDatabase("logibot");
                MongoCollection<Document> collection = database.getCollection("userActivity");
                Document document = collection.find(Filters.and(Filters.eq("userId", userId), Filters.eq("serverId", serverId))).first();
                if (document == null) return Optional.empty();
                Map<Equipment.Type, MutablePair<Integer, Boolean>> map = document.getList("totalSubmits", Document.class).stream().map(doc -> {
                    Equipment.Type type = Equipment.Type.valueOf(doc.getString("equipmentType"));
                    int amount = doc.getInteger("amount");
                    boolean individual = doc.getBoolean("isIndividual");
                    return MutablePair.of(type, MutablePair.of(amount, individual));
                }).collect(Collectors.<MutablePair<Equipment.Type, MutablePair<Integer, Boolean>>, Equipment.Type, MutablePair<Integer, Boolean>>toMap(Pair::getKey, Pair::getValue));
                return Optional.of(new ActivityLog(userId, serverId, map));
            }
        });
    }

    public void calculateTotalSupplyValue() {
        totalSupplyValue = totalSubmits.entrySet().stream().mapToInt(entry -> {
            Equipment.Type type = entry.getKey();
            Pair<Integer, Boolean> pair = entry.getValue();
            int baseValue = type.getSupplyValue();
            int amount = pair.getKey();
            boolean individual = pair.getValue();
            int totalValue = baseValue * amount;
            if (individual && type.getCategory() == Equipment.Category.VEHICLES || type.getCategory() == Equipment.Category.SHIPPABLES) {
                totalValue = totalValue / 3; // All vehicles and shippable crates contain 3 items except for the Falchion but its MPF base cost is still 3x its single cost.
            }
            return totalValue;
        }).sum();
    }

    public List<EmbedBuilder> constructActivityEmbed() {
        ArrayList<EmbedBuilder> embeds = new ArrayList<>();
        EmbedBuilder embedBuilder;
        StringBuilder stringBuilder = new StringBuilder();
        if (totalSubmits.size() != 0) {
            for (List<Map.Entry<Equipment.Type, MutablePair<Integer, Boolean>>> partition : Lists.partition(totalSubmits.entrySet().stream().toList(), 20)) {
                embedBuilder = new EmbedBuilder()
                        .setTitle("Activity Log of " + LogiBot.getApi().getUserById(userId).join().getDisplayName(LogiBot.getApi().getServerById(serverId).get()))
                        .setColor(new Color(35, 40, 145));
                stringBuilder.append("Total supply value: **").append(totalSupplyValue).append("**")
                        .append("\nSummary of all equipment submitted:\n")
                        .append("```\n");
                partition.forEach(entry -> {
                    Equipment.Type type = entry.getKey();
                    Pair<Integer, Boolean> pair = entry.getValue();
                    stringBuilder.append(type.getUserFriendlyName()).append(" x").append(pair.getKey());
                    if (!pair.getValue()) stringBuilder.append(" crate(s)");
                    stringBuilder.append("\n");
                });
                stringBuilder.append("```");
                embedBuilder.setDescription(stringBuilder.toString());
                embeds.add(embedBuilder);
            }
        }
        return embeds;
    }

    @Override
    public Document toDocument() {
        return new Document("userId", userId)
                .append("serverId", serverId)
                .append("totalSubmits", totalSubmits.entrySet().stream().map(entry ->
                        new Document("equipmentType", entry.getKey().toString())
                                .append("amount", entry.getValue().getKey())
                                .append("isIndividual", entry.getValue().getValue())).toList());
    }

    @Override
    public void save() {
        CompletableFuture.runAsync(() -> {
            try (MongoClient client = LogiBot.getDatabaseService().connect()) {
                MongoDatabase database = client.getDatabase("logibot");
                MongoCollection<Document> collection = database.getCollection("userActivity");
                collection.replaceOne(Filters.and(Filters.eq("userId", userId), Filters.eq("serverId", serverId)), this.toDocument(), new ReplaceOptions().upsert(true));
            }
        }).exceptionally(throwable -> {
            throwable.printStackTrace();
            return null;
        });
    }
}
