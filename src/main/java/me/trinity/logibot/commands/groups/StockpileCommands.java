package me.trinity.logibot.commands.groups;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import me.trinity.logibot.LogiBot;
import me.trinity.logibot.commands.Command;
import me.trinity.logibot.commands.CommandGroup;
import me.trinity.logibot.data.equipment.Equipment;
import me.trinity.logibot.data.stockpile.Stockpile;
import me.trinity.logibot.utils.Configuration;
import me.trinity.logibot.utils.StockpileView;
import org.bson.Document;
import org.javacord.api.entity.message.MessageFlag;
import org.javacord.api.entity.message.component.ActionRow;
import org.javacord.api.entity.message.component.Button;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.permission.Role;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;
import org.javacord.api.interaction.SlashCommandOption;
import org.javacord.api.interaction.callback.InteractionFollowupMessageBuilder;

import java.awt.*;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

public class StockpileCommands extends CommandGroup {
    @Override
    public void registerCommands() {
        super.addCommands(
                Command.<Void>builder("createstockpile", "Create a new stockpile to track")
                        .options(SlashCommandOption.createStringOption("name", "Name of the stockpile", true),
                                SlashCommandOption.createStringOption("password", "The password of the stockpile", true),
                                SlashCommandOption.createStringOption("location", "The location of the stockpile", false))
                        .restricted(true)
                        .function(interaction -> CompletableFuture.runAsync(() -> {
                            User user = interaction.getUser();
                            String name = interaction.getArgumentStringValueByName("name").get(); // Should never be null as it is required
                            String location = interaction.getArgumentStringValueByName("location").orElse("?");
                            String password = interaction.getArgumentStringValueByName("password").get();

                            // Very unlikely this would be empty but just to be sure to avoid NPE
                            interaction.getServer().ifPresent(server -> {
                                if (LogiBot.getServerStockpiles().stream().anyMatch(s -> s.getName().equalsIgnoreCase(name) && s.getServerId() == server.getId())) {
                                    interaction.createImmediateResponder().setContent("Stockpile with the name '" + name + "' already exists.").setFlags(MessageFlag.EPHEMERAL).respond();
                                    return;
                                }
                                Stockpile stockpile = new Stockpile(server.getId(), name, user, location, password);
                                LogiBot.getServerStockpiles().add(stockpile);
                                interaction.createImmediateResponder().setContent("Stockpile '" + name + "' has been created.").setFlags(MessageFlag.EPHEMERAL).respond();
                                stockpile.save();
                            });
                        })).build(),
                Command.<Void>builder("deletestockpile", "Deletes a stockpile from the database")
                        .options(SlashCommandOption.createStringOption("name", "Name of the stockpile", true, true))
                        .restricted(true)
                        .function(interaction -> CompletableFuture.runAsync(() -> {
                            String name = interaction.getArgumentStringValueByName("name").get();
                            interaction.getServer().ifPresent(server -> {
                                // Remove the entry from stockpiles list that matches the given predicate. Name will be case-insensitive.
                                LogiBot.getServerStockpiles().stream().filter(stockpile -> stockpile.getName().equalsIgnoreCase(name) && stockpile.getServerId() == server.getId()).findFirst().ifPresentOrElse(stockpile -> {
                                    LogiBot.getServerStockpiles().remove(stockpile);
                                    try (MongoClient client = LogiBot.getDatabaseService().connect()) {
                                        MongoDatabase database = client.getDatabase("logibot");
                                        MongoCollection<Document> collection = database.getCollection("stockpiles");
                                        collection.deleteOne(Filters.and(Filters.eq("serverId", stockpile.getServerId()), Filters.eq("name", stockpile.getName())));
                                    }
                                }, () -> interaction.createImmediateResponder().setContent("Stockpile with the name '" + name + "' was not found.").setFlags(MessageFlag.EPHEMERAL).respond());
                            });
                        })).build(),
                Command.<Void>builder("displaystockpile", "Displays the contents of a specific stockpile, only listing registered equipment")
                        .options(
                                SlashCommandOption.createStringOption("name", "Name of the stockpile", true, true),
                                SlashCommandOption.createBooleanOption("paginated", "Whether to display one category at a time or all at once", false),
                                SlashCommandOption.createBooleanOption("ephemeral", "Whether to display this only to you or publicly. Only applies if paginated", false),
                                SlashCommandOption.createBooleanOption("includeEverything", "Whether to include all contents or just registered equipment", false)
                        )
                        .restricted(false)
                        .function(interaction -> {
                            String name = interaction.getArgumentStringValueByName("name").get();
                            boolean ephemeral = interaction.getArgumentBooleanValueByName("ephemeral").orElse(true);
                            boolean paginated = interaction.getArgumentBooleanValueByName("paginated").orElse(true);
                            boolean full = interaction.getArgumentBooleanValueByName("includeEverything").orElse(false);

                            return interaction.respondLater(ephemeral).thenAccept(updater -> {
                                interaction.getServer().ifPresent(server -> {
                                    if (LogiBot.getServerStockpiles().stream().noneMatch(stockpile -> stockpile.getServerId() == server.getId())) {
                                        interaction.createImmediateResponder().setContent("This server does not have any stockpiles").setFlags(MessageFlag.EPHEMERAL).respond();
                                        return;
                                    }
                                    LogiBot.getServerStockpiles().stream().filter(stockpile -> stockpile.getName().equalsIgnoreCase(name) && stockpile.getServerId() == server.getId()).findFirst().ifPresentOrElse(stockpile -> {
                                        if (paginated) {
                                            StockpileView view = new StockpileView(interaction.getUser(), server, stockpile, Equipment.Category.SMALL_ARMS, full, ephemeral);
                                            LogiBot.getViews().removeIf(userView -> {
                                                if (userView instanceof StockpileView stockpileView) {
                                                    return stockpileView.getUser().getId() == interaction.getUser().getId() && stockpileView.getServer().getId() == server.getId();
                                                }
                                                return false;
                                            });
                                            stockpile.constructCategoryEmbed(Equipment.Category.SMALL_ARMS, full).forEach(updater::addEmbed);
                                            Iterator<EmbedBuilder> iterator = stockpile.constructCategoryEmbed(Equipment.Category.SMALL_ARMS, full).iterator();
                                            while (iterator.hasNext()) {
                                                EmbedBuilder embedBuilder = iterator.next();
                                                InteractionFollowupMessageBuilder messageBuilder = interaction.createFollowupMessageBuilder();
                                                messageBuilder.addEmbed(embedBuilder);
                                                if (!iterator.hasNext()) {
                                                    messageBuilder.addComponents(ActionRow.of(
                                                            Button.primary("stockpilePrevious", "←"),
                                                            Button.primary("stockpileNext", "→")));
                                                }
                                                if (ephemeral) messageBuilder.setFlags(MessageFlag.EPHEMERAL);
                                                messageBuilder.send().thenAccept(message -> view.getMessageIds().add(message.getId()));
                                            }
                                            LogiBot.getViews().add(view);
                                        } else {
                                            stockpile.constructAllCategoriesEmbeds(full).forEach(embedBuilder -> {
                                                InteractionFollowupMessageBuilder messageBuilder = interaction.createFollowupMessageBuilder()
                                                        .addEmbed(embedBuilder);
                                                if (ephemeral) messageBuilder.setFlags(MessageFlag.EPHEMERAL);
                                                messageBuilder.send().join();
                                            });
                                        }
                                    }, () -> interaction.createImmediateResponder().setContent("Stockpile '" + name + "' was not found.").setFlags(MessageFlag.EPHEMERAL).respond());
                                });
                            }).exceptionally(throwable -> {
                                throwable.printStackTrace();
                                return null;
                            });
                        }).build(),
                Command.<Void>builder("viewpassword", "Displays the password of a stockpile.")
                        .options(SlashCommandOption.createStringOption("name", "Name of the stockpile", true, true))
                        .function(interaction -> CompletableFuture.runAsync(() -> {
                            interaction.getServer().ifPresent(server -> {
                                if (!hasMinPasswordRoleOrHigher(interaction.getUser(), server)) {
                                    interaction.createImmediateResponder().addEmbed(new EmbedBuilder()
                                            .setTitle("No permission!")
                                            .setColor(new Color(255, 48, 33))
                                            .setDescription("You're not allowed to run this command!\nIf you believe this to be an error, contact the quartermaster.")
                                    ).setFlags(MessageFlag.EPHEMERAL).respond();
                                    return;
                                }
                                String name = interaction.getArgumentStringValueByName("name").get();
                                LogiBot.getServerStockpiles().stream().filter(stockpile -> stockpile.getName().equalsIgnoreCase(name) && stockpile.getServerId() == server.getId()).findFirst().ifPresentOrElse(stockpile -> {
                                    interaction.createImmediateResponder().addEmbed(new EmbedBuilder().setTitle("Stockpile: " + stockpile.getName())
                                            .addField("Location", stockpile.getLocation())
                                            .addField("Password", stockpile.getPassword()).setColor(new Color(83, 49, 255))).setFlags(MessageFlag.EPHEMERAL).respond();
                                }, () -> interaction.createImmediateResponder().setContent("Stockpile '" + name + "' was not found.").setFlags(MessageFlag.EPHEMERAL).respond());
                            });
                        })).build()
        );
    }

    private boolean hasMinPasswordRoleOrHigher(User user, Server server) {
        Optional<Configuration> optional = LogiBot.getConfigurations().stream().filter(configuration -> configuration.getServerId() == server.getId()).findFirst();
        if (optional.isEmpty()) return false;
        Configuration configuration = optional.get();
        int position = server.getRoleById(configuration.getMinPasswordRole()).map(Role::getPosition).orElse(0);
        int maxPosition = configuration.getRankRoles().stream().map(id -> server.getRoleById(id).orElse(null))
                .filter(Objects::nonNull).map(Role::getPosition)
                .max(Comparator.comparingInt(value -> value)).orElse(0);
        return user.getRoles(server).stream().anyMatch(role -> role.getPosition() >= position && role.getPosition() <= maxPosition);
    }
}
