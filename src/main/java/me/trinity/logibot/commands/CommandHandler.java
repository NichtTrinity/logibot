package me.trinity.logibot.commands;

import org.javacord.api.DiscordApi;
import org.javacord.api.interaction.SlashCommand;
import org.javacord.api.interaction.SlashCommandInteraction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

public class CommandHandler {

    public CommandHandler(DiscordApi api) {
        this.api = api;
        this.commandGroups = new ArrayList<>();
    }

    private final DiscordApi api;
    private final ArrayList<CommandGroup> commandGroups;

    public void addCommandGroup(CommandGroup commandGroup) {
        this.commandGroups.add(commandGroup);
        commandGroup.registerCommands();
        api.getServers().forEach(server -> commandGroup.getCommands().forEach(command -> SlashCommand.with(command.getName(), command.getDescription(), command.getOptions()).setDefaultEnabledForEveryone().createForServer(server).exceptionally(throwable -> {
            throwable.printStackTrace();
            return null;
        })));
    }

    public void registerCommandHandlers() {
        HashMap<String, Function<SlashCommandInteraction, CompletableFuture<?>>> map = new HashMap<>();
        commandGroups.forEach(commandGroup -> commandGroup.getCommands().forEach(command -> map.put(command.getName(), command.getFunction())));
        api.addSlashCommandCreateListener(event -> {
            SlashCommandInteraction interaction = event.getSlashCommandInteraction();
            if (map.containsKey(interaction.getCommandName())) {
                map.get(interaction.getCommandName()).apply(interaction);
            }
        });
    }

}
