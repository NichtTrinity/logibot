package me.trinity.logibot.utils;

import me.trinity.logibot.LogiBot;
import me.trinity.logibot.data.equipment.Equipment;
import me.trinity.logibot.data.stockpile.Stockpile;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class StockpileView implements UserView {

    public StockpileView(User user, Server server, Stockpile stockpile) {
        this(user, server, stockpile, Equipment.Category.SMALL_ARMS, false, true);
    }

    public StockpileView(User user, Server server, Stockpile stockpile, Equipment.Category category, boolean full, boolean ephemeral) {
        this.user = user;
        this.server = server;
        this.stockpile = stockpile;
        this.full = full;
        this.ephemeral = ephemeral;
        this.messageIds = new ArrayList<>();
        this.category = category;
        this.lastUsed = Instant.now();
        this.invalidate();
    }

    private final User user;
    private final Server server;
    private final Stockpile stockpile;
    private final boolean full;
    private final boolean ephemeral;
    private final ArrayList<Long> messageIds;
    private Equipment.Category category;
    private Instant lastUsed;


    @Override
    public User getUser() {
        return this.user;
    }

    @Override
    public Server getServer() {
        return this.server;
    }

    public Equipment.Category getCategory() {
        return this.category;
    }

    public ArrayList<Long> getMessageIds() {
        return messageIds;
    }

    public Stockpile getStockpile() {
        return stockpile;
    }

    public boolean isFull() {
        return full;
    }

    public boolean isEphemeral() {
        return ephemeral;
    }

    public void setCategory(Equipment.Category category) {
        this.category = category;
    }

    public Equipment.Category nextCategory(boolean reversed) {
        this.lastUsed = Instant.now();
        List<Equipment.Category> list = Arrays.asList(Equipment.Category.values());
        int index = list.indexOf(category);
        if (reversed) {
            if (index == 0) index = list.size() - 1;
            else index--;
        } else {
            if (index == list.size() - 1) index = 0;
            else index++;
        }
        Equipment.Category nextCategory = list.get(index);
        this.setCategory(nextCategory);
        return nextCategory;
    }

    @Override
    public void invalidate() {
        if (Duration.between(lastUsed, Instant.now()).toMinutes() >= 30) {
            LogiBot.getViews().remove(this);
        } else {
            LogiBot.getExecutorService().schedule(this::invalidate, 30, TimeUnit.MINUTES);
        }
    }
}
