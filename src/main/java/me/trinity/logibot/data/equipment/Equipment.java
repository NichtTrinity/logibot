package me.trinity.logibot.data.equipment;

import me.trinity.logibot.utils.DatabaseSerializable;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;

import java.util.Arrays;
import java.util.Optional;

public class Equipment implements DatabaseSerializable {

    public Equipment(Type type) {
        this(type, 0, 0, false, false);
    }

    public Equipment(Type type, int amountPresent, int amountRequested, boolean priority, boolean individual) {
        this.type = type;
        this.amountPresent = amountPresent;
        this.amountRequested = amountRequested;
        this.priority = priority;
        this.individual = individual;
    }

    private final Type type;
    private final boolean individual;
    private int amountPresent;
    private int amountRequested;
    private boolean priority;

    public Type getType() {
        return type;
    }

    public int getAmountPresent() {
        return amountPresent;
    }

    public void setAmountPresent(int amountPresent) {
        this.amountPresent = amountPresent;
    }

    public int getAmountRequested() {
        return amountRequested;
    }

    public void setAmountRequested(int amountRequested) {
        this.amountRequested = amountRequested;
    }

    public boolean isPriority() {
        return priority;
    }

    public void setPriority(boolean priority) {
        this.priority = priority;
    }

    public boolean isIndividual() {
        return individual;
    }

    public int calculateAmountNeeded() {
        return this.amountRequested - this.amountPresent;
    }

    @Override
    public Document toDocument() {
        return new Document("equipmentType", this.type.toString())
                .append("amountPresent", this.amountPresent)
                .append("amountRequested", this.amountRequested)
                .append("isPriority", this.priority)
                .append("isIndividual", this.individual);
    }

    public static Equipment fromDocument(Document document) {
        String equipmentType = document.getString("equipmentType");
        try {
            Equipment.Type type = Equipment.Type.valueOf(equipmentType);
            int amountPresent = document.getInteger("amountPresent");
            int amountRequested = document.getInteger("amountRequested");
            boolean priority = document.getBoolean("isPriority");
            boolean individual = document.getBoolean("isIndividual");
            return new Equipment(type, amountPresent, amountRequested, priority, individual);
        } catch (IllegalArgumentException e) {
            System.out.println("Unknown Equipment.Type: " + equipmentType);
            return null;
        }
    }

    @Override
    public void save() {
        // Does nothing
    }

    @Override
    public String toString() {
        return "[" +
                type.toString() + "," +
                amountPresent + "," + amountRequested + "," +
                priority + "," + individual + "]";
    }

    public enum Type {

        // Small Arms
        DUSK("\"Dusk\" ce.III", Category.SMALL_ARMS, StorageType.ITEM, 330, "Dusk"),
        AMMO_7_92("7.92mm", Category.SMALL_ARMS, StorageType.ITEM, 240, "7.92"),
        CATARA("Catara mo.II", Category.SMALL_ARMS, StorageType.ITEM, 330, "Catara", "LMG"),
        GAST_MG("KRN886-127 Gast Machine Gun", Category.SMALL_ARMS, StorageType.ITEM, 500, "Gast", "HMG"),
        BOMASTONE("Bomastone Grenade", Category.SMALL_ARMS, StorageType.ITEM, 400, "Boma", "Bomastone"),
        AMMO_8("8mm", Category.SMALL_ARMS, StorageType.ITEM, 80),
        COMETA("Cometa T2-9", Category.SMALL_ARMS, StorageType.ITEM, 120, "Cometa", "Revolver"),
        AMMO_44(".44", Category.SMALL_ARMS, StorageType.ITEM, 80),
        ARGENTI("Argenti r.II Rifle", Category.SMALL_ARMS, StorageType.ITEM, 200, "Argenti"),
        VOLTA("Volta r.I Repeater", Category.SMALL_ARMS, StorageType.ITEM, 200, "Volta"),
        FUSCINA("Fuscina pi.I", Category.SMALL_ARMS, StorageType.ITEM, 280, "Fuscina"),
        OMEN("KRR2-790 Omen", Category.SMALL_ARMS, StorageType.ITEM, 310, "Omen"),
        AUGER("KRR3-792 Auger", Category.SMALL_ARMS, StorageType.ITEM, 400, "Auger", "Sniper rifle"),
        AMMO_7_62("7.62mm", Category.SMALL_ARMS, StorageType.ITEM, 160, "7.62"),
        SHOTGUN("Brasa Shotgun", Category.SMALL_ARMS, StorageType.ITEM, 160, "Brasa", "Shotgun"),
        AMMO_BUCKSHOT("Buckshot", Category.SMALL_ARMS, StorageType.ITEM, 160, "Shotgun shell"),
        PITCHGUN("\"The Pitch Gun\" mc.V", Category.SMALL_ARMS, StorageType.ITEM, 160, "Pitch Gun"),
        LIONCLAW("\"Lionclaw\" mc.VIII", Category.SMALL_ARMS, StorageType.ITEM, 240, "Lionclaw", "SMG"),
        AMMO_9("9mm", Category.SMALL_ARMS, StorageType.ITEM, 160),
        SMOKE("PT-815 Smoke Grenade", Category.SMALL_ARMS, StorageType.ITEM, 240, "Smoke"),
        GREEN_ASH("Green Ash Grenade", Category.SMALL_ARMS, StorageType.ITEM, 280, "Gas", "Green Ash"),
        AMMO_12_7("12.7mm", Category.SMALL_ARMS, StorageType.ITEM, 200),
        CATENA("Catena rt.IV Auto-Rifle", Category.SMALL_ARMS, StorageType.ITEM, 240, "Catena"),
        BLAKEROW("Blakerow 871", Category.SMALL_ARMS, StorageType.ITEM, 240),
        LOUGHCASTER("No.2 Loughcaster", Category.SMALL_ARMS, StorageType.ITEM, 200),
        SAMPO("Sampo Auto-Rifle 77", Category.SMALL_ARMS, StorageType.ITEM, 250),
        HAWTHORNE("No.2B Hawthorne", Category.SMALL_ARMS, StorageType.ITEM, 140),
        HANGMAN("The Hangman 757", Category.SMALL_ARMS, StorageType.ITEM, 250),
        CLANCY_CINDER("Clancy Cinder M3", Category.SMALL_ARMS, StorageType.ITEM, 260),
        CLANCY_RACA("Clancy-Raca M4", Category.SMALL_ARMS, StorageType.ITEM, 700),
        FIDDLER("Fiddler Submachine Gun Model 868", Category.SMALL_ARMS, StorageType.ITEM, 240),
        LIAR("No.1 \"The Liar\" Submachine Gun", Category.SMALL_ARMS, StorageType.ITEM, 240),
        AALTO("Aalto Storm Rifle 24", Category.SMALL_ARMS, StorageType.ITEM, 330),
        BOOKER("Booker Storm Rifle Model 838", Category.SMALL_ARMS, StorageType.ITEM, 330),
        CASCADIER("Cascadier 873", Category.SMALL_ARMS, StorageType.ITEM, 120),
        MALONE("Malone MK.2", Category.SMALL_ARMS, StorageType.ITEM, 500),
        HARPA("A3 Harpa Fragmentation Grenade", Category.SMALL_ARMS, StorageType.ITEM, 400),

        // Heavy Arms
        TYPHON("\"Typhon\" ra.XII", Category.HEAVY_ARMS, StorageType.ITEM, 300, "Typhon", "ATR"),
        AMMO_20("20mm", Category.HEAVY_ARMS, StorageType.ITEM, 200),
        VENOM("Venom c.II 35", Category.HEAVY_ARMS, StorageType.ITEM, 500, "Venom"),
        BANE("Bane 45", Category.HEAVY_ARMS, StorageType.ITEM, 1100, "Bane"),
        AP_RPG("AP/RPG", Category.HEAVY_ARMS, StorageType.ITEM, 870, "AP Shells"),
        ARC_RPG("ARC/RPG", Category.HEAVY_ARMS, StorageType.ITEM, 870, "ARC Shells"),
        FLAMETHROWER("\"Molten Wind\" v.II Flame Torch", Category.HEAVY_ARMS, StorageType.ITEM, 870, "Flamethrower", "Molten Wild", "Flame Torch", "Flammenwerfer"), // I just had to add it
        LUNAIRE("KLG901-2 Lunaire F", Category.HEAVY_ARMS, StorageType.ITEM, 600, "Lunaire", "Granade Launcher"),
        FISSURA("Mounted Fissura gd.I", Category.HEAVY_ARMS, StorageType.ITEM, 300, "Fissura"),
        TREMOLA("Tremola Grenade GPb-1", Category.HEAVY_ARMS, StorageType.ITEM, 1150, "Tremola"),
        LAMENTUM("Lamentum mm.IV", Category.HEAVY_ARMS, StorageType.ITEM, 300, "Lamentum"),
        DAUCUS("Daucus isg.III", Category.HEAVY_ARMS, StorageType.ITEM, 300, "Daucus", "ISG"),
        AMMO_30("30mm", Category.HEAVY_ARMS, StorageType.ITEM, 560),
        CREMARI("Cremari Mortar", Category.HEAVY_ARMS, StorageType.ITEM, 700, "Mortar", "Handheld mortar"),
        MORTAR_FLARE("Mortar Flare Shell", Category.HEAVY_ARMS, StorageType.ITEM, 220),
        MORTAR_SHRAPNEL("Mortar Shrapnel Shell", Category.HEAVY_ARMS, StorageType.ITEM, 270),
        MORTAR_HE("Mortar Shell", Category.HEAVY_ARMS, StorageType.ITEM, 470),
        IGNIFIST("Ignifist 30", Category.HEAVY_ARMS, StorageType.ITEM, 520, "Igni", "Ignifist"),
        MAMMON("Mammon 91b", Category.HEAVY_ARMS, StorageType.ITEM, 300, "Mammon", "HE"),
        STICKY_BOMB("Anti-Tank Sticky Bomb", Category.HEAVY_ARMS, StorageType.ITEM, 600, "Sticky"),
        RPG("R.P.G. Shell", Category.HEAVY_ARMS, StorageType.ITEM, 570, "RPG Shell"),
        NEVILLE("20 Neville Anti-Tank Rifle", Category.HEAVY_ARMS, StorageType.ITEM, 300),
        RATCATCHER("Malone Ratcatcher MK.1", Category.HEAVY_ARMS, StorageType.ITEM, 300),
        WILLOWS_BANE("Willow's Bane Model 845", Category.HEAVY_ARMS, StorageType.ITEM, 930),
        WHITE_ASH("BF5 White Ash Flask Grenade", Category.HEAVY_ARMS, StorageType.ITEM, 600),
        CUTLER("Cutler Launcher 4", Category.HEAVY_ARMS, StorageType.ITEM, 900),
        BONESAW("Bonesaw MK.3", Category.HEAVY_ARMS, StorageType.ITEM, 700),
        FOEBREAKER("Cutler Foebreaker", Category.HEAVY_ARMS, StorageType.ITEM, 300),
        BONESAW_MOUNTED("Mounted Bonesaw MK.3", Category.HEAVY_ARMS, StorageType.ITEM, 300),

        // Heavy Ammunition
        AMMO_150("150mm", Category.HEAVY_AMMUNITION, StorageType.ITEM, 440),
        AMMO_120("120mm", Category.HEAVY_AMMUNITION, StorageType.ITEM, 270),
        AMMO_250("250mm", Category.HEAVY_AMMUNITION, StorageType.ITEM, 740),
        AMMO_68("68mm AT", Category.HEAVY_AMMUNITION, StorageType.ITEM, 1440),
        AMMO_40("40mm", Category.HEAVY_AMMUNITION, StorageType.ITEM, 1520),
        WARHEAD("Warhead", Category.HEAVY_AMMUNITION, StorageType.ITEM, 24000),

        // Utility
        BARBED_WIRE("Barbed Wire", Category.UTILITY, StorageType.ITEM, 30),
        BINOCULARS("Binoculars", Category.UTILITY, StorageType.ITEM, 150),
        HYDRAS_WHISPER("Hydra's Whisper", Category.UTILITY, StorageType.ITEM, 600, "Hydra"),
        HAVOC_CHARGE("Havoc Charge", Category.UTILITY, StorageType.ITEM, 350, "Havoc"),
        AMMO_FLAMETHROWER("\"Molten Wind\" v.II Ammo", Category.UTILITY, StorageType.ITEM, 420, "Flamethrower ammo", "Flame ammo"),
        LISTENING_KIT("Listening Kit", Category.UTILITY, StorageType.ITEM, 300),
        METAL_BEAM("Metal Beam", Category.UTILITY, StorageType.ITEM, 50, "Beam"),
        RADIO_BACKPACK("Radio Backpack", Category.UTILITY, StorageType.ITEM, 300),
        SANDBAG("Sandbag", Category.UTILITY, StorageType.ITEM, 30),
        HAVOC_DETONATOR("Havoc Charge Detonator", Category.UTILITY, StorageType.ITEM, 250, "Detonator"),
        SHOVEL("Shovel", Category.UTILITY, StorageType.ITEM, 400),
        SLEDGE_HAMMER("Sledge Hammer", Category.UTILITY, StorageType.ITEM, 400),
        LANDMINE("Abisme AT-99", Category.UTILITY, StorageType.ITEM, 300, "Mine", "Landmine", "Abisme"),
        TRIPOD("Tripod", Category.UTILITY, StorageType.ITEM, 200),
        WRENCH("Wrench", Category.UTILITY, StorageType.ITEM, 150),
        WATER_BUCKET("Water Bucket", Category.UTILITY, StorageType.ITEM, 160),
        BAYONET("Buckhorn CCQ-18", Category.UTILITY, StorageType.ITEM, 80, "Bayonet", "Bayo", "Buckhorn"),
        GAS_MASK("Gas Mask", Category.UTILITY, StorageType.ITEM, 320),
        FILTER("Gas Mask Filter", Category.UTILITY, StorageType.ITEM, 200, "Filter"),
        RADIO("Radio", Category.UTILITY, StorageType.ITEM, 150),
        ROCKET_BOOSTER("Rocket Booster", Category.UTILITY, StorageType.ITEM, 16000, "Booster"),
        ALLIGATOR_CHARGE("Alligator Charge", Category.UTILITY, StorageType.ITEM, 1100),
        OSPREAY("The Ospreay", Category.UTILITY, StorageType.ITEM, 370),

        // Resources
        MAINTENANCE_SUPPLIES("Maintenance Supplies", Category.RESOURCES, StorageType.ITEM, 500),
        BASIC_MATERIALS("Basic Materials", Category.RESOURCES, StorageType.MATERIAL, 200, "Bmats"),
        REFINED_MATERIALS("Refined Materials", Category.RESOURCES, StorageType.MATERIAL, 400, "Rmats"),
        EXPLOSIVE_MATERIALS("Explosive Materials", Category.RESOURCES, StorageType.MATERIAL, 200, "Emats"),
        HEAVY_EXPLOSIVE_MATERIALS("Heavy Explosive Materials", Category.RESOURCES, StorageType.MATERIAL, 400, "HEMats"),
        GRAVEL("Gravel", Category.RESOURCES, StorageType.MATERIAL, 50),
        DIESEL("Diesel", Category.RESOURCES, StorageType.ITEM, 10),

        // Medical
        BANDAGES("Bandages", Category.MEDICAL, StorageType.ITEM, 160),
        FIRST_AID_KIT("First Aid Kit", Category.MEDICAL, StorageType.ITEM, 120),
        TRAUMA_KIT("Trauma Kit", Category.MEDICAL, StorageType.ITEM, 160),
        BLOOD_PLASMA("Blood Plasma", Category.MEDICAL, StorageType.ITEM, 160, "Plasma"),
        SOLDIER_SUPPLIES("Soldier Supplies", Category.MEDICAL, StorageType.ITEM, 160, "Shirts"),

        // Uniforms
        FLAK_VEST("Velian Flak Vest", Category.UNIFORMS, StorageType.ITEM, 200, "Flak Vest"),
        ENGINEER_UNIFORM("Fabri Rucksack", Category.UNIFORMS, StorageType.ITEM, 200, "Engineer uniform"),
        GRENADIER_UNIFORM("Grenadier's Baldric", Category.UNIFORMS, StorageType.ITEM, 200, "Grenadier uniform"),
        MEDIC_UNIFORM("Medic Fatigues", Category.UNIFORMS, StorageType.ITEM, 200, "Medic uniform"),
        OFFICER_UNIFORM("Officialis' Attire", Category.UNIFORMS, StorageType.ITEM, 200),
        RAIN_UNIFORM("Legionary's Oilcoat", Category.UNIFORMS, StorageType.ITEM, 200, "Rain uniform", "Rain coat"),
        RECON_UNIFORM("Recon Camo", Category.UNIFORMS, StorageType.ITEM, 200, "Recon uniform"),
        SNOW_UNIFORM("Heavy Topcoat", Category.UNIFORMS, StorageType.ITEM, 200, "Snow uniform", "Snow coat"),
        TANKER_UNIFORM("Tankman's Coveralls", Category.UNIFORMS, StorageType.ITEM, 200, "Tank uniform"),

        // Vehicles
        AMBULANCE("R-12 \"Salus\" Ambulance", Category.VEHICLES, StorageType.VEHICLE, 900, "Ambulance"),
        AC_T3("T3 \"Xiphos\"", Category.VEHICLES, StorageType.VEHICLE, 1500, "T3", "Xiphos"),
        R_15("R-15 - \"Chariot\"", Category.VEHICLES, StorageType.VEHICLE, 600, "Bus", "Battle Bus", "Party Bus"),
        FAT_AA_2("AA-2 Battering Ram", Category.VEHICLES, StorageType.VEHICLE, 1200, "FAT", "Field AT Gun", "68mm Push gun"),
        FG_120_68("120-68 \"Koronides\" Field Gun", Category.VEHICLES, StorageType.VEHICLE, 3000, "Field artillery", "Artillery gun", "120mm push gun", "Field Artillery"),
        FMG_G40("G40 \"Sagittarii\"", Category.VEHICLES, StorageType.VEHICLE, 1200, "FMG", "Field machine gun", "12.7mm push gun"),
        FLATBED("BMS - Packmule Flatbed", Category.VEHICLES, StorageType.VEHICLE, 1800, "Flatbed"),
        R_1("R-1 Hauler", Category.VEHICLES, StorageType.VEHICLE, 600, "Truck"),
        R_5("R-5 \"Atlas\" Hauler", Category.VEHICLES, StorageType.VEHICLE, 720, "Atlas"),
        TYPE_C("Type C - \"Charon\"", Category.VEHICLES, StorageType.VEHICLE, 9600, "Gunboat"),
        ARGONAUT("UV-05a \"Argonaut\"", Category.VEHICLES, StorageType.VEHICLE, 600, "Argonaut"),
        JAVELIN("HH-a \"Javelin\"", Category.VEHICLES, StorageType.VEHICLE, 330, "Javelin"),
        FALCHION("85K-b \"Falchion\"", Category.VEHICLES, StorageType.VEHICLE, 8100, "Falchion"),
        OBRIEN110("O'Brien V.110", Category.VEHICLES, StorageType.VEHICLE, 1500),
        OBRIEN190("O'Brien V.190 Knave", Category.VEHICLES, StorageType.VEHICLE, 800),
        OBRIEN101("O'Brien V.101 Freeman", Category.VEHICLES, StorageType.VEHICLE, 1800),
        OBRIEN113("O'Brien V.113 Gravekeeper", Category.VEHICLES, StorageType.VEHICLE, 1025),
        OBRIEN121("O'Brien V.121 Highlander", Category.VEHICLES, StorageType.VEHICLE, 540),
        OBRIEN130("O'Brien V.130 Wild Jack", Category.VEHICLES, StorageType.VEHICLE, 1025),
        AC_T5("T5 \"Percutio\"", Category.VEHICLES, StorageType.VEHICLE, 925),
        AC_T8("T8 \"Gemini\"", Category.VEHICLES, StorageType.VEHICLE, 1200),
        HOPLITE("HH-b \"Hoplite\"", Category.VEHICLES, StorageType.VEHICLE, 1125),
        PELTAST("HH-d \"Peltast\"", Category.VEHICLES, StorageType.VEHICLE, 1715),
        SKYCALLER("Niska-Rycker Mk. IX Skycaller", Category.VEHICLES, StorageType.VEHICLE, 4410),
        NISKA("Niska Mk. I Gun Motor Carriage", Category.VEHICLES, StorageType.VEHICLE, 1200),
        BLINDER("Niska Mk. II Blinder", Category.VEHICLES, StorageType.VEHICLE, 2677),
        ACHERON("AB-8 \"Acheron\"", Category.VEHICLES, StorageType.VEHICLE, 1200),
        DORU("AB-11 \"Doru\"", Category.VEHICLES, StorageType.VEHICLE, 2260),
        MULLOY("Mulloy LPC", Category.VEHICLES, StorageType.VEHICLE, 1200),
        T12("T12 \"Actaeon\" Tankette", Category.VEHICLES, StorageType.VEHICLE, 2100),
        T13("T13 \"Deioneus\" Tankette", Category.VEHICLES, StorageType.VEHICLE, 4985),
        T14("T14 \"Vesta\" Tankette", Category.VEHICLES, StorageType.VEHICLE, 3262),
        COLLINS_CANNON("Collins Cannon 68mm", Category.VEHICLES, StorageType.VEHICLE, 1200),
        WOLFHOUND("Balfour Wolfhound 40mm", Category.VEHICLES, StorageType.VEHICLE, 1200),
        STYGIAN("945g \"Stygian Bolt\"", Category.VEHICLES, StorageType.VEHICLE, 14950),
        STOCKADE("Balfour Stockade 75mm", Category.VEHICLES, StorageType.VEHICLE, 14950),
        WASP_NEST("Rycker 4/3-F Wasp Nest", Category.VEHICLES, StorageType.VEHICLE, 4352),
        SWALLOWTAIL("Swallowtail 988/127-2", Category.VEHICLES, StorageType.VEHICLE, 1200),
        FALCONER("Balfour Falconer 250mm", Category.VEHICLES, StorageType.VEHICLE, 2100),
        RAMPART("Balfour Rampart 68mm", Category.VEHICLES, StorageType.VEHICLE, 3075),
        SMELTER("40-45 \"Smelter\" Heavy Field Gun", Category.VEHICLES, StorageType.VEHICLE, 3075),
        SPATHA("85K-a \"Spatha\"", Category.VEHICLES, StorageType.VEHICLE, 11925, "Spatha"),
        TALOS("85V-g \"Talos\"", Category.VEHICLES, StorageType.VEHICLE, 16050, "Talos"),
        BARDICHE("86K-a \"Bardiche\"", Category.VEHICLES, StorageType.VEHICLE, 9900, "Bardiche"),
        RANSEUR("86K-c \"Ranseur\"", Category.VEHICLES, StorageType.VEHICLE, 13290, "Ranseur", "Quadiche"),
        SILVERHAND("Silverhand - Mk. IV", Category.VEHICLES, StorageType.VEHICLE, 9300, "Silverhand"),
        CHIEFTAIN("Silverhand Chieftain - MK. VI", Category.VEHICLES, StorageType.VEHICLE, 12727, "Chieftain"),
        LORDSCAR("Silverhand Lordscar - Mk. X", Category.VEHICLES, StorageType.VEHICLE, 14237, "Lordscar", "STD"),
        JUGGERNAUT("Flood Juggernaut Mk. VII", Category.VEHICLES, StorageType.VEHICLE, 228107),
        FLOOD("Flood Mk. I", Category.VEHICLES, StorageType.VEHICLE, 188622),
        OUTLAW("Gallagher Outlaw Mk. II", Category.VEHICLES, StorageType.VEHICLE, 990),
        HIGHWAYMAN("Gallagher Highwayman Mk. III", Category.VEHICLES, StorageType.VEHICLE, 11875),
        THORNFALL("Gallagher Tornfall Mk. VI", Category.VEHICLES, StorageType.VEHICLE, 21337, "Bonelaw"),
        WIDOW("Noble Widow Mk. XIV", Category.VEHICLES, StorageType.VEHICLE, 9600, "HTD"),
        FIREBRAND("Noble Firebrand Mk. XVII", Category.VEHICLES, StorageType.VEHICLE, 13562),
        BALLISTA("HC-7 \"Ballista\"", Category.VEHICLES, StorageType.VEHICLE, 9300),
        SCORPION("HC-2 \"Scorpion\"", Category.VEHICLES, StorageType.VEHICLE, 9487),
        DEVITT("Devitt Mk. III", Category.VEHICLES, StorageType.VEHICLE, 2400),
        IRONHIDE("Devitt Ironhide Mk. IV", Category.VEHICLES, StorageType.VEHICLE, 5422),
        CAINE("Devitt-Caine Mk. IV MMR", Category.VEHICLES, StorageType.VEHICLE, 5175),
        HATCHET("H-5 \"Hatchet\"", Category.VEHICLES, StorageType.VEHICLE, 6900, "Hatchet"),
        KRANESCA("H-8 \"Kranesca\"", Category.VEHICLES, StorageType.VEHICLE, 10425, "Kranesca"),
        VULCAN("H-19 \"Vulcan\"", Category.VEHICLES, StorageType.VEHICLE, 13500, "Vulcan", "SpicyCheese Tank"),
        SPIRE("King Spire Mk. I", Category.VEHICLES, StorageType.VEHICLE, 4200),
        GALLANT("King Gallant Mk. II", Category.VEHICLES, StorageType.VEHICLE, 5525),
        JESTER("King Jester - Mk. I-1", Category.VEHICLES, StorageType.VEHICLE, 8485),
        PREDATOR("Cullen Predator Mk. III", Category.VEHICLES, StorageType.VEHICLE, 904875),
        ARES("O-75b \"Ares\"", Category.VEHICLES, StorageType.VEHICLE, 904875),
        RESPONDER("Dunne Responder 3e", Category.VEHICLES, StorageType.VEHICLE, 900),
        R_12("R-12 - \"Salus\" Ambulance", Category.VEHICLES, StorageType.VEHICLE, 900),
        FUELRUNNER("Dunne Fuelrunner", Category.VEHICLES, StorageType.VEHICLE, 600),
        STOLON("RR-3 \"Stolon\" Tanker", Category.VEHICLES, StorageType.VEHICLE, 600),
        DRUMMOND("Drummond 100a", Category.VEHICLES, StorageType.VEHICLE, 600),
        LOSCANN("Drummond Loscann 55c", Category.VEHICLES, StorageType.VEHICLE, 1037),
        SPITFIRE("Drummond Spitfire 100d", Category.VEHICLES, StorageType.VEHICLE, 1362),
        ICARUS("UV-24 \"Icarus\"", Category.VEHICLES, StorageType.VEHICLE, 1362),
        ODYSSEY("UV-5c \"Odyssey\"", Category.VEHICLES, StorageType.VEHICLE, 712),
        CASTER("03MM \"Caster\"", Category.VEHICLES, StorageType.VEHICLE, 510),
        STINGER("00MS \"Stinger\"", Category.VEHICLES, StorageType.VEHICLE, 697),
        KIVELA("Kivela Power Wheel 80-1", Category.VEHICLES, StorageType.VEHICLE, 510),
        CV("BMS - Universal Assembly Rig", Category.VEHICLES, StorageType.VEHICLE, 600),
        ACV("BMS - Fabricator", Category.VEHICLES, StorageType.VEHICLE, 1925),
        CRANE("BMS - Class 2 Mobile Auto-Crane", Category.VEHICLES, StorageType.VEHICLE, 750),
        DOUSING("Dunne Dousing Engine 3r", Category.VEHICLES, StorageType.VEHICLE, 1787),
        R_12B("R-12b \"Salva\" Flame Truck", Category.VEHICLES, StorageType.VEHICLE, 1787),
        HARVESTER("BMS - Scrap Hauler", Category.VEHICLES, StorageType.VEHICLE, 17987),
        CARAVANER("Dunne Caravaner", Category.VEHICLES, StorageType.VEHICLE, 600),
        R_5B("R-5b \"Sisyphus\" Hauler", Category.VEHICLES, StorageType.VEHICLE, 575),
        R_9("R-9 \"Speartip\"", Category.VEHICLES, StorageType.VEHICLE, 575),
        R_17("R-17 \"Retiarius\" Skirmisher", Category.VEHICLES, StorageType.VEHICLE, 11160),
        DUNNE_TRANSPORT("Dunne Transport", Category.VEHICLES, StorageType.VEHICLE, 200),
        DUNNE_LOADLUGGER("Dunne Loadlugger 3c", Category.VEHICLES, StorageType.VEHICLE, 720),
        DUNNE_LEATHERBACK("Dunne Leatherback 2a", Category.VEHICLES, StorageType.VEHICLE, 575),
        DUNNE_LANDRUNNER("Dunne Landrunner 12c", Category.VEHICLES, StorageType.VEHICLE, 575),
        BICYCLE("Blumfield LK205", Category.VEHICLES, StorageType.NONE, 0),
        AQUATIPPER("BMS - Aquatipper", Category.VEHICLES, StorageType.VEHICLE, 900),
        IRONSHIP("BMS - Ironship", Category.VEHICLES, StorageType.VEHICLE, 3000),
        RONAN("74b-1 Ronan Gunship", Category.VEHICLES, StorageType.VEHICLE, 9600),
        METEORA("74c-2 Ronan Meteora Gunship", Category.VEHICLES, StorageType.VEHICLE, 9600),
        WHITE_WHALE("BMS - White Whale", Category.VEHICLES, StorageType.VEHICLE, 6000),
        GROUPER("BMS - Grouper", Category.VEHICLES, StorageType.NONE, 120),
        HIC("Heavy Infantry Carrier", Category.VEHICLES, StorageType.NONE, 0),
        AFT("Armoured Fighting Tractor", Category.VEHICLES, StorageType.NONE, 0),
        PHALANX("PL-1 \"Phalanx\"", Category.VEHICLES, StorageType.NONE, 0),
        STORM_TANK("Storm Tank", Category.VEHICLES, StorageType.NONE, 0),
        STAFF_CAR("Staff Car", Category.VEHICLES, StorageType.NONE, 0),
        RELIC_TRUCK("Repurposed Truck", Category.VEHICLES, StorageType.NONE, 0),
        CENTURION("Centurion MV-2", Category.VEHICLES, StorageType.NONE, 0),
        SCOURGE_HUNTER("Herne QMW 1a Scourge Hunter", Category.VEHICLES, StorageType.NONE, 0),

        // Shippables
        RESOURCE_CONTAINER("Resource Container", Category.SHIPPABLES, StorageType.SHIPPABLE, 300),
        SHIPPING_CONTAINER("Shipping Container", Category.SHIPPABLES, StorageType.SHIPPABLE, 600),
        MATERIAL_PALLET("Material Pallet", Category.SHIPPABLES, StorageType.SHIPPABLE, 150);


        Type(String userFriendlyName, Category category, StorageType storageType, int supplyValue, String... nicknames) {
            this.userFriendlyName = userFriendlyName;
            this.typeCategory = category;
            this.storageType = storageType;
            this.supplyValue = supplyValue;
            this.nicknames = nicknames;
        }

        private final String userFriendlyName;
        private final Category typeCategory;
        private final StorageType storageType;
        private final String[] nicknames;

        /*
        Calculated by taking its manufacturing cost and multiplying it with the respective conversion rate.
        E.g.: Cost is 100 Basic Materials, which has a conversion rate of 2:1. So to get the supply value we would do 100*2=200.

        Note: This does not represent the logic behind supply value in-game. Currently, the inner-workings of that system are unknown to us. This is simply the easiest I've come up with.

        Cheatsheet:
            - Any raw resource: 1
            - Bmats: 2
            - Rmats: 20
            - Emats: 10
            - HEmats: 20
            - ConMats: 37.5
            - PConMats: 132.5
            - SteelConMats: 697.5
            - AssMats I: 82.5
            - AssMats II: 65
            - AssMats III: 132.5
            - AssMats IV: 242.5
            - AssMats V: 3863.5
         */
        private final int supplyValue;

        public static Optional<Type> fromUserFriendlyName(String s) {
            return Arrays.stream(Type.values()).filter(t -> t.getUserFriendlyName().equals(s)).findFirst();
        }

        public static Optional<Type> fromNickname(String s) {
            return Arrays.stream(Type.values()).filter(type -> Arrays.asList(type.getNicknames()).contains(s)).findFirst();
        }

        public String getUserFriendlyName() {
            return userFriendlyName;
        }

        public Category getCategory() {
            return typeCategory;
        }

        public StorageType getStorageType() {
            return storageType;
        }

        public int getSupplyValue() {
            return supplyValue;
        }

        public String[] getNicknames() {
            return nicknames;
        }

        public enum StorageType {
            VEHICLE(10),
            SHIPPABLE(10),
            ITEM(100),
            MATERIAL(300),
            NONE(0);

            private final int maxStorageAmount;

            StorageType(int maxStorageAmount) {
                this.maxStorageAmount = maxStorageAmount;
            }

            public int getMaxStorageAmount() {
                return maxStorageAmount;
            }
        }
    }

    public enum Category {
        SMALL_ARMS,
        HEAVY_ARMS,
        HEAVY_AMMUNITION,
        UTILITY,
        MEDICAL,
        RESOURCES,
        UNIFORMS,
        VEHICLES,
        SHIPPABLES;

        public static Category fromUserFriendlyName(String s) {
            return Category.valueOf(s.toUpperCase().replace(" ", "_"));
        }

        // Returns the name of the category with 1st letter of each word in uppercase
        public String getUserFriendlyName() {
            return StringUtils.capitalize(this.toString().toLowerCase().replace("_", " "));
        }
    }

}
