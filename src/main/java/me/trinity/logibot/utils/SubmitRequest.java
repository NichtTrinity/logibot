package me.trinity.logibot.utils;

import me.trinity.logibot.LogiBot;
import me.trinity.logibot.data.equipment.Equipment;
import me.trinity.logibot.data.stockpile.Stockpile;
import org.apache.commons.lang3.tuple.MutablePair;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class SubmitRequest implements UserView {

    private final User user;
    private final Instant creationTime;
    private final Server server;
    private final Stockpile stockpile;
    private Map<MutablePair<Equipment.Type, Boolean>, Integer> submitList;

    public SubmitRequest(User user, Server server, Stockpile stockpile) {
        this.user = user;
        this.server = server;
        this.stockpile = stockpile;
        this.submitList = new HashMap<>();
        this.creationTime = Instant.now();
        this.invalidate();
    }

    public User getUser() {
        return user;
    }

    public Instant getCreationTime() {
        return creationTime;
    }

    public Server getServer() {
        return server;
    }

    public Stockpile getStockpile() {
        return stockpile;
    }

    public Map<MutablePair<Equipment.Type, Boolean>, Integer> getSubmitList() {
        return submitList;
    }

    public void setSubmitList(Map<MutablePair<Equipment.Type, Boolean>, Integer> submitList) {
        this.submitList = submitList;
    }

    @Override
    public String toString() {
        return stockpile.getName() + "(" + server.getId() + ") target stockpile is " + stockpile.getName() + ": " + submitList.toString();
    }

    @Override
    public void invalidate() {
        if (Duration.between(creationTime, Instant.now()).toMinutes() >= 15) {
            LogiBot.getViews().remove(this);
        } else {
            LogiBot.getExecutorService().schedule(this::invalidate, 15, TimeUnit.MINUTES);
        }
    }
}
