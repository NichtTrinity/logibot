package me.trinity.logibot.commands;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;

public abstract class CommandGroup {

    protected CommandGroup() {
        this.commands = new ArrayList<>();
    }

    @Getter
    private final ArrayList<Command> commands;

    protected void addCommand(Command command) {
        this.commands.add(command);
    }

    protected void addCommands(Command... commands) {
        this.commands.addAll(Arrays.asList(commands));
    }

    public abstract void registerCommands();

}
