package me.trinity.logibot.utils;

import java.util.Random;

public class ProgressBar {

    public static String create(int length, Color color) {
        String emoji;
        String placeholder = ":black_large_square:";
        switch (color) {
            case BLUE -> emoji = ":blue_square:";
            case BROWN -> emoji = ":brown_square:";
            case GREEN -> emoji = ":green_square:";
            case ORANGE -> emoji = ":orange_square:";
            case PURPLE -> emoji = ":purple_square:";
            case RED -> emoji = ":red_square:";
            case YELLOW -> emoji = ":yellow_square:";
            default -> emoji = ":white_large_square:";
        }

        int count = 10 - length;
        if (length > 10) length = 10;
        if (count < 0) count = 0;
        return emoji.repeat(length) + placeholder.repeat(count);
    }

    public enum Color {
        WHITE,
        BLUE,
        BROWN,
        GREEN,
        ORANGE,
        PURPLE,
        RED,
        YELLOW;

        public static Color random() {
            Random random = new Random();
            return Color.values()[random.nextInt(Color.values().length)];
        }
    }

}
