package me.trinity.logibot.utils;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.ReplaceOptions;
import lombok.Getter;
import lombok.Setter;
import me.trinity.logibot.LogiBot;
import org.bson.Document;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Getter
@Setter
public class Configuration implements DatabaseSerializable {

    public Configuration(long serverId, List<Long> botmasters, List<Long> rankRoles, long minPasswordRole) {
        this.serverId = serverId;
        this.botmasters = botmasters;
        this.rankRoles = rankRoles;
        this.minPasswordRole = minPasswordRole;
    }

    private final long serverId;
    private final List<Long> botmasters;
    private final List<Long> rankRoles;
    private long minPasswordRole;

    @Override
    public Document toDocument() {
        return new Document("serverId", serverId)
                .append("botmasters", botmasters)
                .append("rankRoles", rankRoles)
                .append("minPasswordRole", minPasswordRole);
    }

    @Override
    public void save() {
        CompletableFuture.runAsync(() -> {
            try (MongoClient client = LogiBot.getDatabaseService().connect()) {
                MongoDatabase database = client.getDatabase("logibot");
                MongoCollection<Document> collection = database.getCollection("configurations");
                collection.replaceOne(Filters.eq("serverId", serverId), this.toDocument(), new ReplaceOptions().upsert(true));
            }
        });
    }
}
