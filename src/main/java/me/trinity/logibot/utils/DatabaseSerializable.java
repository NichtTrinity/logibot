package me.trinity.logibot.utils;

import org.bson.Document;

public interface DatabaseSerializable {

    Document toDocument();

    void save();

}
