package me.trinity.logibot.data.equipment;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.ReplaceOptions;
import lombok.Getter;
import lombok.Setter;
import me.trinity.logibot.LogiBot;
import me.trinity.logibot.utils.DatabaseSerializable;
import me.trinity.logibot.utils.RandomIdGenerator;
import org.bson.Document;
import org.javacord.api.entity.message.embed.EmbedBuilder;

import java.awt.*;
import java.util.concurrent.CompletableFuture;

@Getter
@Setter
public class SpecialOrder implements DatabaseSerializable {

    public SpecialOrder(Equipment equipment, String message, Priority priority, long serverId, long userId) {
        this.equipment = equipment;
        this.message = message;
        this.priority = priority;
        this.serverId = serverId;
        this.userId = userId;
        this.id = RandomIdGenerator.generateId(4);
    }

    public SpecialOrder(long serverId, long userId, String id, Priority priority, String message, Equipment equipment) {
        this.serverId = serverId;
        this.userId = userId;
        this.id = id;
        this.priority = priority;
        this.message = message;
        this.equipment = equipment;
    }

    private final long serverId;
    private final long userId;
    private String id;
    private Priority priority;
    private String message;
    private final Equipment equipment;

    public EmbedBuilder constructEmbed() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("**Requested equipment**:\n")
                .append("`").append(equipment.getType().getUserFriendlyName()).append(" x").append(equipment.getAmountRequested());
        if (!equipment.isIndividual()) stringBuilder.append(" Crate(s)");
        stringBuilder.append("`\n\n")
                .append("**Priority**: ***").append(priority.toString()).append("***");
        if (!message.isEmpty()) stringBuilder.append("\n**Information provided**: ").append(message);
        return new EmbedBuilder()
                .setTitle("Special Order (" + id + ")")
                .setColor(priority.getColor())
                .setDescription(stringBuilder.toString())
                .setAuthor(LogiBot.getApi().getUserById(userId).join());
    }

    public EmbedBuilder constructCompleteEmbed() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("**ORDER COMPLETED**\n\n")
                .append("*Requested equipment*: ")
                .append("`").append(equipment.getType().getUserFriendlyName()).append(" x").append(equipment.getAmountPresent());
        if (!equipment.isIndividual()) stringBuilder.append(" Crate(s)");
        stringBuilder.append("`\n\n")
                .append("*Priority*: *").append(priority.toString()).append("*");
        return new EmbedBuilder()
                .setTitle("Special Order (" + id + ")")
                .setColor(Color.GREEN)
                .setDescription(stringBuilder.toString())
                .setAuthor(LogiBot.getApi().getUserById(userId).join());
    }

    @Override
    public Document toDocument() {
        return new Document("serverId", serverId)
                .append("id", id)
                .append("userId", userId)
                .append("priority", priority.toString())
                .append("message", message)
                .append("equipment", equipment.toDocument());
    }

    @Override
    public void save() {
        CompletableFuture.runAsync(() -> {
            try (MongoClient client = LogiBot.getDatabaseService().connect()) {
                MongoDatabase database = client.getDatabase("logibot");
                MongoCollection<Document> collection = database.getCollection("specialorders");
                collection.replaceOne(Filters.and(Filters.eq("serverId", serverId), Filters.eq("id", id)),
                        this.toDocument(), new ReplaceOptions().upsert(true));
            }
        });
    }

    public void delete() {
        CompletableFuture.runAsync(() -> {
            try (MongoClient client = LogiBot.getDatabaseService().connect()) {
                MongoDatabase database = client.getDatabase("logibot");
                MongoCollection<Document> collection = database.getCollection("specialorders");
                collection.deleteOne(Filters.and(Filters.eq("id", id), Filters.eq("serverId", serverId)));
            }
            LogiBot.getSpecialOrders().remove(this);
        });
    }

    public enum Priority {
        LOW(new Color(0, 135, 27)),
        NORMAL(new Color(247, 231, 0)),
        HIGH(new Color(247, 115, 0)),
        URGENT(new Color(247, 40, 40)),
        EMERGENCY(new Color(168, 0, 0));

        Priority(Color color) {
            this.color = color;
        }

        @Getter
        private final Color color;

    }

}
