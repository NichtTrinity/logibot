package me.trinity.logibot.utils;

import me.trinity.logibot.LogiBot;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ActivityView implements UserView {

    public ActivityView(User user, Server server, List<EmbedBuilder> embeds) {
        this.user = user;
        this.server = server;
        this.embeds = embeds;
        this.currentIndex = -1;
        this.lastUsed = Instant.now();
        this.invalidate();
    }

    private final User user;
    private final Server server;
    private final List<EmbedBuilder> embeds;
    private int currentIndex;
    private Instant lastUsed;

    public EmbedBuilder nextPage(boolean reversed) {
        this.lastUsed = Instant.now();
        if (reversed) {
            currentIndex = currentIndex == 0 ? embeds.size() - 1 : --currentIndex;
        } else {
            currentIndex = currentIndex == embeds.size() - 1 ? 0 : ++currentIndex;
        }
        return embeds.get(currentIndex);
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public Server getServer() {
        return server;
    }

    @Override
    public void invalidate() {
        if (Duration.between(lastUsed, Instant.now()).toMinutes() >= 10) {
            LogiBot.getViews().remove(this);
        } else {
            LogiBot.getExecutorService().schedule(this::invalidate, 30, TimeUnit.MINUTES);
        }
    }
}
