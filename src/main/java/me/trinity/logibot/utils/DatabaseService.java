package me.trinity.logibot.utils;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

public class DatabaseService {

    public DatabaseService(String uri) {
        this.uri = uri;
    }

    private final String uri;

    public MongoClient connect() {
        return MongoClients.create(uri);
    }

}
