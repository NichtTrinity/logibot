package me.trinity.logibot.commands;

import com.google.common.base.Preconditions;
import lombok.Getter;
import me.trinity.logibot.LogiBot;
import me.trinity.logibot.utils.BuilderBase;
import org.javacord.api.entity.message.MessageFlag;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.interaction.SlashCommandInteraction;
import org.javacord.api.interaction.SlashCommandOption;
import org.javacord.api.interaction.callback.InteractionFollowupMessageBuilder;

import javax.imageio.ImageIO;
import javax.naming.NoPermissionException;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

@Getter
public final class Command<R> {

    public Command(String name, String description, List<SlashCommandOption> options, boolean restrictedToBotmasters, boolean experimental, Function<SlashCommandInteraction, CompletableFuture<R>> function) {
        this.name = name;
        this.description = description;
        this.options = options;
        this.restrictedToBotmasters = restrictedToBotmasters;
        this.experimental = experimental;
        this.function = interaction -> {
            if (restrictedToBotmasters && !LogiBot.isBotmaster(interaction.getUser(), interaction.getServer().get())) {
                interaction.createImmediateResponder().addEmbed(new EmbedBuilder()
                        .setTitle("No permission!")
                        .setColor(new Color(255, 48, 33))
                        .setDescription("You're not allowed to run this command!\nIf you believe this to be an error, contact the quartermaster.")
                ).setFlags(MessageFlag.EPHEMERAL).respond();
                return CompletableFuture.failedFuture(new NoPermissionException());
            }
            return function.apply(interaction).whenCompleteAsync((r, throwable) -> {
                if (throwable != null) {
                    throwable.printStackTrace();
                    return;
                }
                if (experimental) {
                    BufferedImage image = null;
                    try {
                        image = ImageIO.read(new URL("https://static.wikia.nocookie.net/foxhole_gamepedia_en/images/7/73/Icon_Blueprint.png"));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    EmbedBuilder embedBuilder = new EmbedBuilder()
                            .setColor(Color.YELLOW)
                            .setTitle("Experimental Feature")
                            .setDescription("This feature is under active development. If you encounter any issues, please report them.");
                    if (image != null) embedBuilder.setThumbnail(image);
                    try {
                        Thread.sleep(1000);
                        InteractionFollowupMessageBuilder messageBuilder = interaction.createFollowupMessageBuilder().addEmbed(embedBuilder);
                        if (r instanceof Boolean b && b) {
                            messageBuilder.setFlags(MessageFlag.EPHEMERAL);
                        }
                        messageBuilder.send();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        };
    }

    private final String name;
    private final String description;
    private final List<SlashCommandOption> options;
    private final boolean restrictedToBotmasters;
    private final boolean experimental;
    private final Function<SlashCommandInteraction, CompletableFuture<R>> function;

    public static <R> Builder<R> builder() {
        return new Builder<R>();
    }

    public static <R> Builder<R> builder(String name, String description) {
        return new Builder<R>(name, description);
    }

    public static class Builder<R> implements BuilderBase<Command<R>> {

        private Builder() {
        }

        private Builder(String name, String description) {
            this.name = name;
            this.description = description;
        }

        private String name;
        private String description;
        private boolean restricted;
        private boolean experimental;
        private List<SlashCommandOption> options;
        private Function<SlashCommandInteraction, CompletableFuture<R>> function;

        public Builder<R> name(String name) {
            this.name = name;
            return this;
        }

        public Builder<R> description(String description) {
            this.description = description;
            return this;
        }

        public Builder<R> restricted(boolean restricted) {
            this.restricted = restricted;
            return this;
        }

        public Builder<R> experimental(boolean experimental) {
            this.experimental = experimental;
            return this;
        }

        public Builder<R> options(SlashCommandOption... options) {
            this.options = Arrays.asList(options);
            return this;
        }

        public Builder<R> function(Function<SlashCommandInteraction, CompletableFuture<R>> function) {
            this.function = function;
            return this;
        }

        @Override
        public Command<R> build() {
            Preconditions.checkNotNull(name, "Name cannot be null!");
            Preconditions.checkNotNull(description, "Description cannot be null!");
            Preconditions.checkNotNull(options, "Options cannot be null!");
            Preconditions.checkNotNull(function, "Handler cannot be null!");

            return new Command<>(name, description, options, restricted, experimental, function);
        }

    }

}
