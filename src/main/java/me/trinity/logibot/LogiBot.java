package me.trinity.logibot;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.Getter;
import me.trinity.logibot.commands.CommandGroup;
import me.trinity.logibot.commands.CommandHandler;
import me.trinity.logibot.commands.groups.EquipmentCommands;
import me.trinity.logibot.commands.groups.MiscCommands;
import me.trinity.logibot.commands.groups.StockpileCommands;
import me.trinity.logibot.data.equipment.Equipment;
import me.trinity.logibot.data.equipment.SpecialOrder;
import me.trinity.logibot.data.stockpile.Stockpile;
import me.trinity.logibot.data.user.ActivityLog;
import me.trinity.logibot.utils.*;
import me.xdrop.fuzzywuzzy.FuzzySearch;
import me.xdrop.fuzzywuzzy.model.ExtractedResult;
import org.apache.commons.lang3.tuple.MutablePair;
import org.bson.Document;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.activity.ActivityType;
import org.javacord.api.entity.intent.Intent;
import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.entity.message.MessageFlag;
import org.javacord.api.entity.message.MessageUpdater;
import org.javacord.api.entity.message.component.Button;
import org.javacord.api.entity.message.component.*;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.message.mention.AllowedMentionsBuilder;
import org.javacord.api.entity.permission.PermissionState;
import org.javacord.api.entity.permission.PermissionType;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;
import org.javacord.api.interaction.*;
import org.javacord.api.interaction.callback.ComponentInteractionOriginalMessageUpdater;
import org.javacord.api.interaction.callback.InteractionFollowupMessageBuilder;
import org.javacord.api.interaction.callback.InteractionMessageBuilder;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.util.List;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class LogiBot {

    @Getter
    private static DiscordApi api;
    @Getter
    private static ObjectMapper mapper;
    @Getter
    private static ArrayList<Configuration> configurations;
    @Getter
    private static ArrayList<Stockpile> serverStockpiles;
    @Getter
    private static ArrayList<SpecialOrder> specialOrders;
    @Getter
    private static DatabaseService databaseService;
    @Getter
    private static CommandHandler commandHandler;

    @Getter
    private static ArrayList<UserView> views;
    @Getter
    private static ScheduledExecutorService executorService;

    public static void main(String[] args) throws IOException {
        mapper = new ObjectMapper();
        createConfigFile();
        JsonNode json = mapper.readTree(new File("config.json"));
        api = new DiscordApiBuilder()
                .setToken(json.get("token").asText())
                .setIntents(Intent.GUILDS, Intent.GUILD_MEMBERS, Intent.GUILD_EMOJIS, Intent.GUILD_PRESENCES, Intent.GUILD_MESSAGES, Intent.GUILD_MESSAGE_TYPING, Intent.GUILD_MESSAGE_REACTIONS, Intent.MESSAGE_CONTENT)
                .login().join();
        Map<String, ActivityType> map = Map.of(
                "Managing logistics...", ActivityType.PLAYING,
                "Stockpile levels...", ActivityType.WATCHING,
                "a scrooping contest", ActivityType.COMPETING,
                "with mass extinction weapons", ActivityType.PLAYING,
                "Fleshwound-senpai", ActivityType.WATCHING
        );

        Runtime.getRuntime().addShutdownHook(new Thread(() -> api.disconnect().join())); // Gracefully shuts down when receiving terminate signal

        configurations = new ArrayList<>();
        serverStockpiles = new ArrayList<>();
        specialOrders = new ArrayList<>();
        databaseService = new DatabaseService(json.get("databaseUrl").asText());
        commandHandler = new CommandHandler(api);
        views = new ArrayList<>();
        executorService = Executors.newSingleThreadScheduledExecutor();
        loadStockpileData().join(); // Block until everything is loaded
        loadSpecialOrders().join();
        loadConfiguration().join();

        executorService.scheduleAtFixedRate(() -> randomizeActivityStatus(map), 0, 15, TimeUnit.MINUTES);

        final int FUZZY_CUTOFF = 75;

        CommandGroup[] commandGroups = {
                new StockpileCommands(),
                new EquipmentCommands(),
                new MiscCommands()
        };
        for (CommandGroup commandGroup : commandGroups) {
            commandHandler.addCommandGroup(commandGroup);
        }
        commandHandler.registerCommandHandlers();

        // Autocomplete Listeners
        api.addAutocompleteCreateListener(event -> {
            CompletableFuture.runAsync(() -> {
                AutocompleteInteraction interaction = event.getAutocompleteInteraction();
                interaction.getServer().ifPresent(server -> {
                    switch (interaction.getCommandName()) {
                        case "addequipment", "removeequipment", "editequipment", "displaystockpile", "deletestockpile", "submitequipment", "specialorder", "inspect", "viewpassword" -> {
                            switch (interaction.getFocusedOption().getName()) {
                                case "name" ->
                                        interaction.respondWithChoices(serverStockpiles.stream().filter(stockpile -> stockpile.getServerId() == server.getId()).map(Stockpile::getName).map(s -> SlashCommandOptionChoice.create(s, s)).toList());
                                case "equipmenttype" -> {
                                    List<Equipment.Type> choices = Arrays.asList(Equipment.Type.values());
                                    List<Equipment.Type> filtered = FuzzySearch.extractSorted(interaction.getFocusedOption().getStringValue().get(), choices.stream().map(Equipment.Type::getUserFriendlyName)
                                            .toList()).stream().map(extractedResult -> Equipment.Type.fromUserFriendlyName(extractedResult.getString()).get()).toList();
                                    if (filtered.size() > 25) {
                                        filtered = filtered.subList(0, 25);
                                    }
                                    interaction.respondWithChoices(filtered.stream().map(type -> SlashCommandOptionChoice.create(type.getUserFriendlyName(), type.toString())).toList());
                                }
                                case "id" -> {
                                    List<String> choices = specialOrders.stream().map(SpecialOrder::getId).toList();
                                    List<String> filtered = FuzzySearch.extractSorted(interaction.getFocusedOption().getStringValue().get(), choices).stream().map(ExtractedResult::getString).toList();
                                    if (filtered.size() > 25) {
                                        filtered = filtered.subList(0, 25);
                                    }
                                    interaction.respondWithChoices(filtered.stream().map(s -> SlashCommandOptionChoice.create(s, s)).toList());
                                }
                            }
                        }
                    }
                });
            }).orTimeout(10, TimeUnit.MINUTES);
        });

        // Modal Listeners
        api.addModalSubmitListener(event -> {
            CompletableFuture.runAsync(() -> {
                ModalInteraction interaction = event.getModalInteraction();
                switch (interaction.getCustomId()) {
                    case "bulkmodal" -> {
                        String paragraph = interaction.getTextInputValueByCustomId("bulkmodal_input").get(); // It's always required, never blank.
                        if (!paragraph.isBlank()) { // redundant
                            try {
                                // Split the paragraph by line feeds.
                                // Flatbed 2c .... Flatbed 2
                                String[] lines = paragraph.translateEscapes().split("\n");

                                // Prepare a filter for the parser. 2 substrings minimum, last one needs to be a number and a number bigger than 1
                                List<String> badInput = new ArrayList<>(Arrays.stream(lines)
                                        .filter(line -> {
                                            String[] parts = line.split(" ");

                                            // A waste of space, but it's readable and safe.
                                            boolean condition1 = parts.length >= 2; // 2 substring at least
                                            boolean condition2 = condition1 && parts[0].length() > 3; // first one needs to be bigger than 3 chars to not make the fuzzy go crazy
                                            boolean condition3 = condition2 && parts[parts.length - 1].matches("\\d+|\\d+c|\\d+C"); // the last one needs to be a number or a number + c or number + C
                                            boolean condition4 = condition3 && parts[parts.length - 1].length() < 5; // but small amount to not overflow
                                            boolean condition5 = condition4 && Integer.parseInt((parts[parts.length - 1]).replaceAll("[cC]", "")) >= 1; // and not negative for it to be a logical amount


                                            return !(condition1 && condition2 && condition3 && condition4 && condition5);
                                        })
                                        .toList());

                                // Filters out the bad input. Then parses the line into 2 elements: a name and an amount.
                                Map<MutablePair<String, Boolean>, Integer> parsedInput = Arrays.stream(lines)
                                        .filter(line -> !badInput.contains(line))
                                        .map(line -> {
                                            String[] parts = line.split(" ");
                                            String equipmentName = String.join(" ", Arrays.copyOfRange(parts, 0, parts.length - 1));
                                            String lastPart = parts[parts.length - 1];
                                            boolean isIndividual = !(lastPart.endsWith("c") || lastPart.endsWith("C"));
                                            int amount = Integer.parseInt(lastPart.replaceAll("[cC]", ""));
                                            MutablePair<String, Boolean> equipmentPair = MutablePair.of(equipmentName, isIndividual);
                                            return MutablePair.of(equipmentPair, amount);
                                        })
                                        .collect(Collectors.toMap(
                                                MutablePair::getLeft,
                                                MutablePair::getRight,
                                                Integer::sum
                                        ));
                                System.out.println("Parsed: " + parsedInput);
                                System.out.println("Bad Input: " + badInput);
                                // Each string is processed by the fuzzy algorithm. Then filters out strings under the FUZZY_CUTOFF threshold.
                                List<Equipment.Type> choices = Arrays.asList(Equipment.Type.values());
                                Map<MutablePair<Equipment.Type, Boolean>, Integer> filtered = parsedInput.entrySet().stream()
                                        .flatMap(entry -> {
                                            MutablePair<String, Boolean> equipmentPair = entry.getKey();
                                            int amount = entry.getValue();

                                            String equipmentName = equipmentPair.getLeft();
                                            boolean isIndividual = equipmentPair.getRight();

                                            // First fuzzy, using UserFriendlyName.
                                            List<String> choicesName = choices.stream().map(Equipment.Type::getUserFriendlyName).toList();
                                            ExtractedResult extractedResult = FuzzySearch.extractOne(equipmentName, choicesName);
                                            Optional<Equipment.Type> fuzzyResult = Equipment.Type.fromUserFriendlyName(extractedResult.getString());

                                            // First score check, which is 15% stricter than the cutoff, ensuring the input is properly typed.
                                            if (extractedResult.getScore() < Math.max(0, Math.min(100, FUZZY_CUTOFF + 15))) {
                                                // Second fuzzy if the first one fails, uses Nicknames instead.
                                                List<String> choicesNicknames = Arrays.stream(Equipment.Type.values())
                                                        .flatMap(type -> Arrays.stream(type.getNicknames()))
                                                        .toList();
                                                extractedResult = FuzzySearch.extractOne(equipmentName, choicesNicknames);
                                                fuzzyResult = Equipment.Type.fromNickname(extractedResult.getString());

                                                // Second score check, default cutoff, if it fails to pass, then it's added to the badInput list.
                                                if (extractedResult.getScore() < FUZZY_CUTOFF || fuzzyResult.isEmpty()) {
                                                    badInput.add(equipmentName + " " + amount);
                                                }
                                            }

                                            ExtractedResult finalExtractedResult = extractedResult;
                                            return fuzzyResult.filter(result -> finalExtractedResult.getScore() > FUZZY_CUTOFF)
                                                    .map(type -> {
                                                        // Hardcoded, anything not under vehicle/shippable category gets overwritten to crated variants
                                                        boolean isShippableOrVehicle = type.getCategory() == Equipment.Category.SHIPPABLES || type.getCategory() == Equipment.Category.VEHICLES;
                                                        boolean updatedIsIndividual = isShippableOrVehicle ? isIndividual : false;
                                                        return Map.entry(MutablePair.of(type, updatedIsIndividual), amount);
                                                    })
                                                    .stream();
                                        })
                                        .collect(Collectors.toMap(
                                                Map.Entry::getKey,
                                                Map.Entry::getValue,
                                                Integer::sum
                                        ));

                                // Generates a MessageComponent with 3 options (Confirm,Edit,Cancel) and an embed displaying what the fuzzy understood. TODO: Pretty me please
                                EmbedBuilder eb = new EmbedBuilder().setTitle("Equipment submission algorithm results:");
                                ActionRow buttons;
                                if (!filtered.isEmpty()) {
                                    // If something was parsed successfully, get the submitrequest from the user+server ids, add the parsed equipment types and amounts.
                                    interaction.getServer().ifPresent(server -> {
                                        views.stream().filter(view -> view instanceof SubmitRequest).map(view -> (SubmitRequest) view)
                                                .filter(request -> request.getUser().getId() == interaction.getUser().getId() && request.getServer().getId() == server.getId()).findFirst()
                                                .ifPresentOrElse(request -> request.setSubmitList(filtered), () -> {
                                                    // This scenario shouldn't be possible unless the server abruptly crashes as every submit request is added to the queue before this modal listener.
                                                    // TODO: Verify that this makes sense
                                                    interaction.createImmediateResponder().setContent("Something went wrong, please try again").setFlags(MessageFlag.EPHEMERAL).respond();
                                                });
                                    });

                                    List<String> friendlyFiltered = filtered.entrySet().stream()
                                            .map(entry -> entry.getKey().getLeft().getUserFriendlyName() + " x" + entry.getValue() + (entry.getKey().getRight() ? " (unit)" : ""))
                                            .toList();
                                    eb.setColor(Color.green).addField("Valid input", "Understood (Matched with " + FUZZY_CUTOFF + "% Precision):\n```" + String.join("\n", friendlyFiltered) + "```");
                                    eb.setFooter("This submit request will be valid for the next 15 minutes. If you don't confirm or cancel the request you'll need to make a new one");
                                    buttons = ActionRow.of(
                                            Button.create("bulkmodal_button_confirm", ButtonStyle.SUCCESS, "Confirm"),
                                            Button.create("bulkmodal_button_edit", ButtonStyle.SECONDARY, "Edit"),
                                            Button.create("bulkmodal_button_cancel", ButtonStyle.DANGER, "Cancel")
                                    );
                                } else {
                                    buttons = ActionRow.of(
                                            Button.create("bulkmodal_button_edit", ButtonStyle.SECONDARY, "Edit"),
                                            Button.create("bulkmodal_button_cancel", ButtonStyle.DANGER, "Cancel")
                                    );
                                }
                                if (!badInput.isEmpty()) {
                                    eb.setColor(Color.red).addField("Invalid input", "Couldn't understand the following input(s):\n```" + String.join("\n", badInput) + "```");
                                    eb.setFooter("Remember to follow the input format. The amount has to be greater than 1 but smaller than 999 and if you wish to specify a vehicle or shippable is crated then add a single 'c' next to the amount. Equipment name too vague or not known, or too short.");
                                }

                                interaction.createImmediateResponder().addEmbed(eb).addComponents(buttons).setFlags(MessageFlag.EPHEMERAL).respond();
                            } catch (Exception e) {
                                System.err.println(e.getMessage());
                                e.printStackTrace();
                                interaction.createImmediateResponder().setContent("Something went wrong...\n" + e.getMessage()).setFlags(MessageFlag.EPHEMERAL).respond();
                            }

                        }
                    }
                }
            }).orTimeout(10, TimeUnit.MINUTES);
        });

        api.addMessageComponentCreateListener(event -> {
            CompletableFuture.runAsync(() -> {
                MessageComponentInteraction interaction = event.getMessageComponentInteraction();
                String id = interaction.getCustomId();
                ComponentInteractionOriginalMessageUpdater updater = interaction.createOriginalMessageUpdater();
                ActionRow actionRow = ActionRow.of(
                        Button.primary("stockpilePrevious", "←"),
                        Button.primary("stockpileNext", "→")
                );

                switch (id) {
                    case "bulkmodal_button_confirm" -> {
                        interaction.getServer().ifPresent(server -> {
                            views.stream().filter(view -> view instanceof SubmitRequest).map(view -> (SubmitRequest) view)
                                    .filter(request -> request.getUser().getId() == interaction.getUser().getId() && request.getServer().getId() == server.getId())
                                    .findFirst().ifPresentOrElse(request -> {
                                        request.getSubmitList().forEach((key, value) -> {
                                            request.getStockpile().submitEquipment(key.getLeft(), value, key.getRight()); // TODO: add parsing of individual items for vehicles and shippables
                                        });
                                        request.getStockpile().save();
                                        ActivityLog.load(request.getUser(), server).thenAccept(optional -> {
                                            ActivityLog activityLog = optional.orElse(new ActivityLog(request.getUser(), server));
                                            request.getSubmitList().forEach((key, value) -> {
                                                if (activityLog.getTotalSubmits().containsKey(key)) {
                                                    MutablePair<Integer, Boolean> pair = activityLog.getTotalSubmits().get(key);
                                                    pair.setLeft(pair.getLeft() + value);
                                                } else {
                                                    activityLog.getTotalSubmits().put(key.getLeft(), MutablePair.of(value, false)); // TODO: add support for individual items
                                                }
                                            });
                                            activityLog.save();
                                        });
                                        System.out.println(request.getSubmitList().toString());
                                        views.remove(request);
                                        EmbedBuilder embedBuilder = new EmbedBuilder();
                                        String fieldBuilder = "```" +
                                                String.join("\n", request.getSubmitList().entrySet().stream()
                                                        .map(entry -> entry.getKey().getLeft().getUserFriendlyName() + " x" + entry.getValue() + (entry.getKey().getRight() ? " (unit)" : ""))
                                                        .toArray(String[]::new)) +
                                                "```";

                                        embedBuilder.setTitle("Equipment successfully added to the stockpile!").setColor(Color.green)
                                                .addField("Here's the list of equipment you've submitted:", fieldBuilder);
                                        updater.removeAllEmbeds().addEmbed(embedBuilder).setFlags(MessageFlag.EPHEMERAL).removeAllComponents();
                                        interaction.createFollowupMessageBuilder().addEmbed(new EmbedBuilder()
                                                        .setColor(Color.GREEN)
                                                        .setDescription(interaction.getUser().getMentionTag() + " submitted equipment to Stockpile '" + request.getStockpile().getName() + "'\n" + fieldBuilder))
                                                .setAllowedMentions(new AllowedMentionsBuilder().addUser(interaction.getUser().getId()).build()).send();
                                    }, () -> interaction.createImmediateResponder().setContent("You have no pending submit requests!").setFlags(MessageFlag.EPHEMERAL).respond());
                        });
                    }
                    case "bulkmodal_button_edit" -> {
                        interaction.getServer().ifPresent(server -> {
                            views.stream().filter(view -> view instanceof SubmitRequest).map(view -> (SubmitRequest) view)
                                    .filter(request -> request.getUser().getId() == interaction.getUser().getId() && request.getServer().getId() == server.getId()).findFirst()
                                    .ifPresentOrElse(request -> {
                                        String s = "";
                                        if (!request.getSubmitList().isEmpty())
                                            s = request.getSubmitList().entrySet().stream().map(entry -> entry.getKey().getLeft().getUserFriendlyName() + " " + entry.getValue() + (entry.getKey().getRight() ? "" : "c") + "\n").collect(Collectors.joining());
                                        interaction.respondWithModal("bulkmodal", "Submit your equipment in bulk!",
                                                ActionRow.of(TextInput.create(TextInputStyle.PARAGRAPH, "bulkmodal_input",
                                                        "Submit to \"" + request.getStockpile().getName() + "\" :",
                                                        "Input format: <equipment name> <amount>\nCatara 5\nbmats 3\nHauler 1c\n...", s, true)));
                                    }, () -> interaction.createImmediateResponder().setContent("You have no pending submit requests!").setFlags(MessageFlag.EPHEMERAL).respond());
                        });
                    }
                    case "bulkmodal_button_cancel" -> {
                        interaction.getServer().ifPresent(server -> {
                            views.stream().filter(view -> view instanceof SubmitRequest).map(view -> (SubmitRequest) view)
                                    .filter(request -> request.getUser().getId() == interaction.getUser().getId() && request.getServer().getId() == server.getId()).findFirst()
                                    .ifPresentOrElse(request -> {
                                        views.remove(request);
                                        EmbedBuilder embedBuilder = new EmbedBuilder().setTitle("Equipment submission canceled.");
                                        embedBuilder.setColor(Color.red).setDescription("Your request has been canceled, if you wish to submit equipment again please use `/submitequipment` again.");
                                        updater.removeAllEmbeds().addEmbed(embedBuilder).setFlags(MessageFlag.EPHEMERAL).removeAllComponents();
                                    }, () -> interaction.createImmediateResponder().setContent("You have no pending submit requests!").setFlags(MessageFlag.EPHEMERAL).respond());
                        });
                    }
                    case "stockpilePrevious" -> {
                        views.stream().filter(view -> view instanceof StockpileView && view.getUser().getId() == interaction.getUser().getId())
                                .map(view -> (StockpileView) view).findFirst().or(() -> views.stream().filter(view -> view instanceof StockpileView)
                                        .map(view -> (StockpileView) view).filter(view -> view.getStockpile().getName().equals(interaction.getMessage().getEmbeds().get(0).getTitle().get().replace("Stockpile: ", ""))).findFirst())
                                .ifPresent(view -> {
                                    interaction.getServer().ifPresent(server -> {
                                        updater.removeAllEmbeds();
                                        InteractionMessageBuilder builder = new InteractionMessageBuilder();
                                        List<EmbedBuilder> embeds;
                                        do {
                                            Equipment.Category category = view.nextCategory(true);
                                            embeds = view.getStockpile().constructCategoryEmbed(category, view.isFull());
                                        } while (embeds.isEmpty());

                                        if (embeds.size() < view.getMessageIds().size()) {
                                            for (int i = embeds.size(); i < view.getMessageIds().size(); i++) {
                                                builder.deleteFollowupMessage(interaction, String.valueOf(view.getMessageIds().get(i))).exceptionally(throwable -> {
                                                    throwable.printStackTrace();
                                                    return null;
                                                });
                                            }
                                        }
                                        ArrayList<Long> list = new ArrayList<>();
                                        for (int i = 0; i < embeds.size(); i++) {
                                            EmbedBuilder embedBuilder = embeds.get(i);
                                            if (i < view.getMessageIds().size()) {
                                                long messageId = view.getMessageIds().get(i);
                                                MessageUpdater messageUpdater = builder.editFollowupMessage(interaction, messageId).join().createUpdater();
                                                messageUpdater.addEmbed(embedBuilder);
                                                if (++i >= embeds.size()) {
                                                    messageUpdater.addComponents(ActionRow.of(
                                                            Button.primary("stockpilePrevious", "←"),
                                                            Button.primary("stockpileNext", "→")));
                                                }
                                                messageUpdater.replaceMessage().thenAccept(message -> list.add(message.getId()));
                                            } else {
                                                InteractionFollowupMessageBuilder messageBuilder = interaction.createFollowupMessageBuilder();
                                                messageBuilder.addEmbed(embedBuilder);
                                                if (++i >= embeds.size()) {
                                                    messageBuilder.addComponents(ActionRow.of(
                                                            Button.primary("stockpilePrevious", "←"),
                                                            Button.primary("stockpileNext", "→")));
                                                }
                                                if (view.isEphemeral()) messageBuilder.setFlags(MessageFlag.EPHEMERAL);
                                                messageBuilder.send().thenAccept(message -> list.add(message.getId()));
                                            }
                                        }
                                        view.getMessageIds().clear();
                                        view.getMessageIds().addAll(list);
                                        embeds.forEach(updater::addEmbed);
                                        updater.addComponents(actionRow);
                                    });
                                });
                    }
                    case "stockpileNext" -> {
                        views.stream().filter(view -> view instanceof StockpileView && view.getUser().getId() == interaction.getUser().getId())
                                .map(view -> (StockpileView) view).findFirst().or(() -> views.stream().filter(view -> view instanceof StockpileView)
                                        .map(view -> (StockpileView) view).filter(view -> view.getStockpile().getName().equals(interaction.getMessage().getEmbeds().get(0).getTitle().get().replace("Stockpile: ", ""))).findFirst())
                                .ifPresent(view -> {
                                    updater.removeAllEmbeds();
                                    List<EmbedBuilder> embeds;
                                    do {
                                        Equipment.Category category = view.nextCategory(false);
                                        embeds = view.getStockpile().constructCategoryEmbed(category, view.isFull());
                                    } while (embeds.isEmpty());
                                    embeds.forEach(updater::addEmbed);
                                    updater.addComponents(actionRow);
                                });
                    }
                    case "activityPrevious" -> {
                        views.stream().filter(view -> view instanceof ActivityView && view.getUser().getId() == interaction.getUser().getId())
                                .map(view -> (ActivityView) view).findFirst().ifPresent(view -> {
                                    updater.removeAllEmbeds();
                                    updater.addEmbed(view.nextPage(true));
                                    updater.addComponents(ActionRow.of(
                                            Button.primary("activityPrevious", "←"),
                                            Button.primary("activityNext", "→")));
                                });
                    }
                    case "activityNext" -> {
                        views.stream().filter(view -> view instanceof ActivityView && view.getUser().getId() == interaction.getUser().getId())
                                .map(view -> (ActivityView) view).findFirst().ifPresent(view -> {
                                    updater.removeAllEmbeds();
                                    updater.addEmbed(view.nextPage(false));
                                    updater.addComponents(ActionRow.of(
                                            Button.primary("activityPrevious", "←"),
                                            Button.primary("activityNext", "→")));
                                });
                    }
                    case "roleSelectionConfirm" -> {
                        interaction.getServer().ifPresent(server -> {
                            views.stream().filter(userView -> userView instanceof RoleSelection && userView.getUser().getId() == interaction.getUser().getId() && userView.getServer().getId() == server.getId()).map(userView -> (RoleSelection) userView).findFirst().ifPresent(roleSelection -> {
                                InteractionMessageBuilder builder = new InteractionMessageBuilder();
                                roleSelection.getMessageIds().forEach(messageId -> builder.deleteFollowupMessage(interaction, String.valueOf(messageId)));
                                ArrayList<Long> list = new ArrayList<>();
                                roleSelection.getRoles().values().forEach(roles -> roles.forEach(role -> {
                                    if (!list.contains(role.getId())) list.add(role.getId());
                                }));
                                LogiBot.getConfigurations().stream().filter(configuration -> configuration.getServerId() == server.getId()).findFirst()
                                        .ifPresent(configuration -> {
                                            configuration.getRankRoles().clear();
                                            configuration.getRankRoles().addAll(list);
                                            configuration.save();
                                        });
                                interaction.acknowledge();
                            });
                        });
                    }
                    case "roleSelectionCancel" -> {
                        interaction.getServer().ifPresent(server -> {
                            views.stream().filter(userView -> userView instanceof RoleSelection && userView.getUser().getId() == interaction.getUser().getId() && userView.getServer().getId() == server.getId()).map(userView -> (RoleSelection) userView).findFirst().ifPresent(roleSelection -> {
                                InteractionMessageBuilder builder = new InteractionMessageBuilder();
                                roleSelection.getMessageIds().forEach(messageId -> builder.deleteFollowupMessage(interaction, String.valueOf(messageId)));
                                LogiBot.getViews().remove(roleSelection);
                                interaction.acknowledge();
                            });
                        });
                    }
                }
                updater.update();
            }).orTimeout(10, TimeUnit.MINUTES);
        });
        api.addMessageCreateListener(event -> {
            event.getMessage().getUserAuthor().ifPresent(user -> {
                String message = event.getMessageContent();
                Pattern pattern = Pattern.compile("(sing up)|(ammount)|(regex is fun)", Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(message);
                while (matcher.find()) {
                    switch (matcher.group().toLowerCase()) {
                        case "ammount" ->
                                event.getMessage().getChannel().sendMessage("It's **amount**, " + event.getMessage().getUserAuthor().get().getMentionTag() + "! Jesus fuck...");
                        case "sing up" -> event.getMessage().addReaction("🎶");
                        case "regex is fun" -> {
                            try {
                                new MessageBuilder()
                                        .setContent("No. No it's not...")
                                        .addAttachment(new URL("https://media.tenor.com/X1S5OFcU8SUAAAAi/withered-wojack.gif"))
                                        .replyTo(event.getMessageId())
                                        .send(event.getChannel());
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });
        });
        api.addSelectMenuChooseListener(event -> CompletableFuture.runAsync(() -> {
            SelectMenuInteraction interaction = event.getSelectMenuInteraction();
            interaction.getServer().ifPresent(server -> {
                if (interaction.getCustomId().startsWith("roleSelection")) {
                    int index = Integer.parseInt(interaction.getCustomId().replace("roleSelection", ""));
                    views.stream().filter(userView -> userView instanceof RoleSelection && userView.getUser().getId() == interaction.getUser().getId() && userView.getServer().getId() == server.getId()).map(userView -> (RoleSelection) userView).findFirst().ifPresent(roleSelection -> {
                        roleSelection.getRoles().put(index, interaction.getSelectedRoles());
                    });
                }
            });
        }));
    }

    public static boolean isBotmaster(User user, Server server) {
        AtomicBoolean atomicBoolean = new AtomicBoolean();
        configurations.stream().filter(configuration -> configuration.getServerId() == server.getId()).findFirst()
                .ifPresent(configuration -> {
                    if (configuration.getBotmasters().isEmpty()) {
                        atomicBoolean.set(user.getRoles(server).stream().anyMatch(role -> role.getPermissions().getState(PermissionType.ADMINISTRATOR) == PermissionState.ALLOWED)); // When no botmasters are set, fall back to administrator permission check
                    }
                    atomicBoolean.set(configuration.getBotmasters().contains(user.getId()));
                });
        return atomicBoolean.get();
    }

    private static void createConfigFile() throws IOException {
        File file = new File("config.json");
        if (!file.exists()) {
            try (InputStream in = LogiBot.class.getResourceAsStream("/config.json")) {
                Files.copy(in, file.toPath());
            } catch (IOException e) {
                file.createNewFile();
                JsonGenerator generator = mapper.createGenerator(file, JsonEncoding.UTF8);
                generator.writeStartObject();
                generator.writeStringField("token", "TOKEN");
                generator.writeStringField("databaseUrl", "url");
                generator.writeEndObject();
                generator.close();
            }
        }
    }

    public static CompletableFuture<Void> loadStockpileData() {
        return CompletableFuture.runAsync(() -> {
            try (MongoClient client = databaseService.connect()) {
                MongoDatabase database = client.getDatabase("logibot");
                MongoCollection<Document> collection = database.getCollection("stockpiles");
                collection.find().forEach(document -> {
                    long serverId = document.getLong("serverId");
                    String name = document.getString("name");
                    String location = document.getString("location");
                    String password = document.getString("password");
                    long creatorId = document.getLong("creatorId");
                    List<Equipment> contents = document.getList("contents", Document.class).stream().map(Equipment::fromDocument)
                            .filter(Objects::nonNull).toList();
                    Stockpile stockpile = new Stockpile(serverId, name, creatorId, location, password, contents);
                    serverStockpiles.add(stockpile);
                });
            }
        }).exceptionally(throwable -> {
            throwable.printStackTrace();
            return null;
        });
    }

    public static CompletableFuture<Void> loadSpecialOrders() {
        return CompletableFuture.runAsync(() -> {
            try (MongoClient client = databaseService.connect()) {
                MongoDatabase database = client.getDatabase("logibot");
                MongoCollection<Document> collection = database.getCollection("specialorders");
                collection.find().forEach(document -> {
                    long serverId = document.getLong("serverId");
                    long userId = document.getLong("userId");
                    String id = document.getString("id");
                    SpecialOrder.Priority priority = SpecialOrder.Priority.valueOf(document.getString("priority"));
                    String message = document.getString("message");
                    Equipment equipment = Equipment.fromDocument(document.get("equipment", Document.class));

                    SpecialOrder specialOrder = new SpecialOrder(serverId, userId, id, priority, message, equipment);
                    specialOrders.add(specialOrder);
                });
            }
        }).exceptionally(throwable -> {
            throwable.printStackTrace();
            return null;
        });
    }

    public static CompletableFuture<Void> loadConfiguration() {
        return CompletableFuture.runAsync(() -> {
            try (MongoClient client = databaseService.connect()) {
                MongoDatabase database = client.getDatabase("logibot");
                MongoCollection<Document> collection = database.getCollection("configurations");
                collection.find().forEach(document -> {
                    long serverId = document.getLong("serverId");
                    List<Long> botmasters = document.getList("botmasters", Long.class);
                    List<Long> rankRoles = document.getList("rankRoles", Long.class);
                    long minPasswordRole = document.getLong("minPasswordRole");

                    configurations.add(new Configuration(serverId, botmasters, rankRoles, minPasswordRole));
                });
            }
            api.getServers().forEach(server -> {
                if (configurations.stream().noneMatch(configuration -> configuration.getServerId() == server.getId())) {
                    Configuration configuration = new Configuration(server.getId(), new ArrayList<>(), new ArrayList<>(), 0);
                    configuration.save();
                    configurations.add(configuration); // Add empty config for servers that don't have one yet.
                }
            });
        });
    }

    public static void randomizeActivityStatus(Map<String, ActivityType> map) {
        Random random = new Random();
        List<Map.Entry<String, ActivityType>> list = List.copyOf(map.entrySet());
        Map.Entry<String, ActivityType> entry = list.get(random.nextInt(list.size()));
        api.updateActivity(entry.getValue(), entry.getKey());
    }

}
