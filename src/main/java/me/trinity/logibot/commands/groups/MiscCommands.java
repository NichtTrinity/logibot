package me.trinity.logibot.commands.groups;

import com.fasterxml.jackson.databind.JsonNode;
import me.trinity.logibot.LogiBot;
import me.trinity.logibot.commands.Command;
import me.trinity.logibot.commands.CommandGroup;
import me.trinity.logibot.data.equipment.Equipment;
import me.trinity.logibot.data.user.ActivityLog;
import me.trinity.logibot.utils.ActivityView;
import me.trinity.logibot.utils.RoleSelection;
import org.apache.commons.lang3.tuple.MutablePair;
import org.javacord.api.entity.message.MessageFlag;
import org.javacord.api.entity.message.component.ActionRow;
import org.javacord.api.entity.message.component.Button;
import org.javacord.api.entity.message.component.ComponentType;
import org.javacord.api.entity.message.component.SelectMenuBuilder;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.permission.Role;
import org.javacord.api.entity.user.User;
import org.javacord.api.interaction.SlashCommandOption;
import org.javacord.api.interaction.callback.InteractionImmediateResponseBuilder;
import org.javacord.api.util.logging.ExceptionLogger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MiscCommands extends CommandGroup {
    @Override
    public void registerCommands() {
        super.addCommands(
                Command.<Void>builder("refreshcache", "Invalidates the stockpile cache and forces an update from the database")
                        .options()
                        .restricted(true)
                        .function(interaction -> CompletableFuture.runAsync(() -> {
                            interaction.getServer().ifPresent(server -> {
                                LogiBot.getServerStockpiles().clear();
                                interaction.createImmediateResponder().setContent("Rebuilding cache...").setFlags(MessageFlag.EPHEMERAL).respond().thenAccept(updater -> {
                                    LogiBot.loadStockpileData().thenRun(() -> updater.setContent("Stockpile cache successfully rebuild.").update());
                                });
                            });
                        })).build(),
                Command.<Void>builder("activitylog", "Display information about the logistics activity of a member")
                        .options(SlashCommandOption.createUserOption("user", "The user to display information about", true))
                        .restricted(true)
                        .function(interaction -> {
                            User user = interaction.getArgumentUserValueByName("user").get();
                            return interaction.respondLater(true).thenAccept(updater -> {
                                interaction.getServer().ifPresent(server -> {
                                    ActivityLog.load(user, server).thenAccept(optional -> {
                                        optional.ifPresentOrElse(activityLog -> {
                                            List<EmbedBuilder> embeds = activityLog.constructActivityEmbed();
                                            if (embeds.size() > 1) {
                                                ActivityView view = new ActivityView(user, server, embeds);
                                                LogiBot.getViews().add(view);
                                                ActionRow actionRow = ActionRow.of(
                                                        Button.primary("activityPrevious", "←"),
                                                        Button.primary("activityNext", "→")
                                                );
                                                updater.addEmbed(view.nextPage(false)).addComponents(actionRow);
                                            } else if (embeds.size() == 1) {
                                                updater.addEmbed(embeds.get(0));
                                            } else {
                                                updater.setContent("There is no activity data about this user.");
                                            }
                                        }, () -> updater.setContent("There is no activity data about this user."));
                                    }).join();
                                    updater.update();
                                });
                            }).exceptionally(throwable -> {
                                throwable.printStackTrace();
                                return null;
                            });
                        }).build(),
                Command.<Void>builder("activityranking", "Displays the top members in terms of supply value")
                        .options()
                        .restricted(true)
                        .function(interaction -> interaction.respondLater(true).thenAccept(updater -> {
                            interaction.getServer().ifPresent(server -> {
                                EmbedBuilder embedBuilder = new EmbedBuilder()
                                        .setTitle("Top 10 Logi members")
                                        .setColor(new Color(31, 255, 184));
                                server.getMembers().stream().map(user -> ActivityLog.load(user, server).join())
                                        .map(optional -> optional.orElse(null))
                                        .filter(Objects::nonNull)
                                        .sorted(Comparator.comparingInt(ActivityLog::getTotalSupplyValue).reversed())
                                        .limit(10)
                                        .forEach(activityLog -> {
                                            int totalAmountSubmits = activityLog.getTotalSubmits().values().stream().mapToInt(MutablePair::getLeft).sum();
                                            LogiBot.getApi().getUserById(activityLog.getUserId()).thenAccept(user -> embedBuilder.addField(user.getNickname(server).orElse(user.getName()), "```\nTotal Supply Value: " + activityLog.getTotalSupplyValue() + "\nTotal amount of submitted equipment: " + totalAmountSubmits + "```"));
                                        });
                                updater.addEmbed(embedBuilder).update();
                            });
                        })).build(),
                Command.<Boolean>builder("inspect", "Display basic information about an equipment")
                        .options(SlashCommandOption.createStringOption("equipmentType", "The name of the equipment", true, true),
                                SlashCommandOption.createBooleanOption("ephemeral", "Whether to display this only to you or publicly.", false))
                        .experimental(true)
                        .function(interaction -> CompletableFuture.supplyAsync(() -> {
                            String name = interaction.getArgumentStringValueByName("equipmentType").get();
                            boolean ephemeral = interaction.getArgumentBooleanValueByName("ephemeral").orElse(true);
                            interaction.respondLater(ephemeral).thenAccept(updater -> {
                                EmbedBuilder error = new EmbedBuilder()
                                        .setTitle("An error occurred")
                                        .setDescription("The request couldn't be processed, please try again in a few minutes")
                                        .setColor(Color.RED);

                                try {
                                    String description = null;
                                    String descriptionExtra = null;
                                    BufferedImage image = null;

                                    Equipment.Type type = Equipment.Type.valueOf(name);
                                    HttpClient client = HttpClient.newHttpClient();
                                    HttpRequest request;
                                    String title = type.getUserFriendlyName().replace(" ", "+").replace("\"", "%22");
                                    String content;
                                    String body;
                                    JsonNode node;
                                    int index = 0;
                                    int tries = 0;
                                    do {
                                        if (tries >= 15) {
                                            throw new InterruptedException("Too many tries.");
                                        }
                                        request = HttpRequest.newBuilder()
                                                .version(HttpClient.Version.HTTP_2)
                                                .uri(URI.create("https://foxhole.wiki.gg/api.php?action=query&prop=revisions&titles=" + title + "&rvslots=*&rvprop=content&formatversion=2&format=json"))
                                                .GET()
                                                .build();
                                        body = client.send(request, HttpResponse.BodyHandlers.ofString()).body();
                                        node = LogiBot.getMapper().readTree(body);
                                        boolean missing = node.get("query").get("pages").get(0).get("missing") != null;
                                        if (missing) {
                                            if (index >= type.getNicknames().length) {
                                                title = type.toString();
                                                continue;
                                            }
                                            title = type.getNicknames()[index].replace(" ", "+").replace("\"", "%22");
                                            index++;
                                            continue;
                                        }
                                        content = node.get("query").get("pages").get(0).get("revisions").get(0).get("slots").get("main").get("content").toString();
                                        if (content.contains("#REDIRECT")) {
                                            title = content.replace("#REDIRECT ", "").replace("[", "")
                                                    .replace("]", "").replace(" ", "+").replace("\"", "");
                                        } else break;
                                        tries++;
                                    } while (true);

                                    Pattern pattern = Pattern.compile("(description\\}\\}\\\\n\\\\n.+?\\\\n\\\\n=)", Pattern.MULTILINE);
                                    Matcher matcher = pattern.matcher(content);
                                    if (matcher.find()) {
                                        description = matcher.group(0).replace("}}", "").replace("\\n", "\n").replace("description", "")
                                                .replace("[", "").replace("]", "").replace("=", "")
                                                .replace("Factions|", "").replace("\"\"", "**").replace("\n", "")
                                                .replace("'''", "**").replace("<code>", "`").replace("</code>", "`");
                                        pattern = Pattern.compile("(Description.==.+?=.+?)", Pattern.MULTILINE);
                                        matcher = pattern.matcher(content);
                                        if (matcher.find()) {
                                            descriptionExtra = matcher.group(0).replace("==", "").replace("Description", "")
                                                    .replace("\\n", "\n").replace("[", "").replace("]", "")
                                                    .replace("Factions|", "").replace("\"\"", "**").replace("'''", "**").replace("<code>", "`").replace("</code>", "`");
                                        }
                                        pattern = Pattern.compile("(\\|image = .*?.\\|)", Pattern.MULTILINE);
                                        matcher = pattern.matcher(content);
                                        if (matcher.find()) {
                                            String imageName = matcher.group(0).replace("image = ", "").replace("|", "").replace("\\n", "").replace(" ", "_");
                                            request = HttpRequest.newBuilder()
                                                    .version(HttpClient.Version.HTTP_2)
                                                    .uri(URI.create("https://foxhole.wiki.gg/api.php?action=query&prop=imageinfo&titles=File:" + imageName + "&iiprop=url&formatversion=2&format=json"))
                                                    .GET()
                                                    .build();
                                            body = client.send(request, HttpResponse.BodyHandlers.ofString()).body();
                                            node = LogiBot.getMapper().readTree(body);
                                            String url = node.get("query").get("pages").get(0).get("imageinfo").get(0).get("url").asText();
                                            image = ImageIO.read(new URL(url));
                                        }
                                    }

                                    if (description != null) {
                                        StringBuilder builder = new StringBuilder()
                                                .append(description);
                                        if (descriptionExtra != null)
                                            builder.append("\n\n").append(descriptionExtra);
                                        EmbedBuilder embedBuilder = new EmbedBuilder()
                                                .setTitle(type.getUserFriendlyName())
                                                .setDescription(builder.toString())
                                                .setColor(new Color(255, 130, 28));
                                        if (image != null) embedBuilder.setThumbnail(image);
                                        updater.addEmbed(embedBuilder);
                                    } else {
                                        updater.addEmbed(error);
                                    }
                                    updater.update();
                                } catch (IllegalArgumentException | IOException | InterruptedException e) {
                                    e.printStackTrace();
                                    updater.addEmbed(error).update();
                                }
                            }).join();
                            return ephemeral;
                        })).build(),
                Command.<Void>builder("configure", "Configurate the bot settings")
                        .options(
                                SlashCommandOption.createSubcommand("addbotmaster", "Declares a user as a botmaster. Botmasters are allowed to run restricted commands.", Collections.singletonList(SlashCommandOption.createUserOption("user", "The user to add.", true))),
                                SlashCommandOption.createSubcommand("removebotmaster", "Revokes botmaster status of a user", Collections.singletonList(SlashCommandOption.createUserOption("user", "The user to remove", true))),
                                SlashCommandOption.createSubcommand("setrankroles", "Defines the roles that are considered regiment ranks. These will be used in permission checks.", Collections.singletonList(SlashCommandOption.createDecimalOption("numberOfRoles", "How many roles you plan to select. This is used to work around Discord's limit of 25 per menu.", true))),
                                SlashCommandOption.createSubcommand("setminpasswordrole", "Defines the minimum role/rank required to view the stockpile passwords", Collections.singletonList(SlashCommandOption.createRoleOption("role", "The role required", true)))
                        ).restricted(true)
                        .function(interaction -> CompletableFuture.runAsync(() -> {
                            interaction.getServer().ifPresent(server -> {
                                switch (interaction.getOptions().get(0).getName()) {
                                    case "addbotmaster" -> {
                                        User user = interaction.getArgumentUserValueByName("user").get();
                                        LogiBot.getConfigurations().stream().filter(configuration -> configuration.getServerId() == server.getId()).findFirst()
                                                .ifPresent(configuration -> {
                                                    InteractionImmediateResponseBuilder responseBuilder = interaction.createImmediateResponder();
                                                    if (configuration.getBotmasters().contains(user.getId())) {
                                                        responseBuilder.setContent("This user is already a botmaster.").setFlags(MessageFlag.EPHEMERAL).respond();
                                                        return;
                                                    }
                                                    configuration.getBotmasters().add(user.getId());
                                                    configuration.save();
                                                    responseBuilder.setContent("User '" + user.getNickname(server) + "' is now a botmaster.").respond();
                                                });
                                    }
                                    case "removebotmaster" -> {
                                        User user = interaction.getArgumentUserValueByName("user").get();
                                        LogiBot.getConfigurations().stream().filter(configuration -> configuration.getServerId() == server.getId()).findFirst()
                                                .ifPresent(configuration -> {
                                                    InteractionImmediateResponseBuilder responseBuilder = interaction.createImmediateResponder();
                                                    if (!configuration.getBotmasters().contains(user.getId())) {
                                                        responseBuilder.setContent("This user is not a botmaster.").setFlags(MessageFlag.EPHEMERAL).respond();
                                                        return;
                                                    }
                                                    configuration.getBotmasters().remove(user.getId());
                                                    configuration.save();
                                                    responseBuilder.setContent("User '" + user.getNickname(server) + "' is no longer a botmaster.").respond();
                                                });
                                    }
                                    case "setrankroles" -> {
                                        interaction.respondLater(true).thenAccept(updater -> {
                                            float numberOfRoles = interaction.getArgumentDecimalValueByName("numberOfRoles").get().floatValue();
                                            int selectMenus = (int) Math.ceil(numberOfRoles / 25);
                                            LogiBot.getViews().removeIf(userView -> userView instanceof RoleSelection && userView.getUser().getId() == interaction.getUser().getId() && userView.getServer().getId() == server.getId());
                                            RoleSelection roleSelection = new RoleSelection(interaction.getUser(), server);
                                            LogiBot.getViews().add(roleSelection);
                                            for (int i = 0; i < selectMenus; i++) {
                                                interaction.createFollowupMessageBuilder().addComponents(ActionRow.of(new SelectMenuBuilder(ComponentType.SELECT_MENU_ROLE, "roleSelection" + i)
                                                        .setMaximumValues(25).build())).setFlags(MessageFlag.EPHEMERAL).send().thenAccept(message -> roleSelection.getMessageIds().add(message.getId())).exceptionally(ExceptionLogger.get());
                                            }
                                            interaction.createFollowupMessageBuilder().addComponents(ActionRow.of(
                                                    Button.success("roleSelectionConfirm", "Confirm"),
                                                    Button.danger("roleSelectionCancel", "Cancel")
                                            )).setFlags(MessageFlag.EPHEMERAL).send().thenAccept(message -> roleSelection.getMessageIds().add(message.getId()));
                                        });
                                    }
                                    case "setminpasswordrole" -> {
                                        Role role = interaction.getArgumentRoleValueByName("role").get();
                                        LogiBot.getConfigurations().stream().filter(configuration -> configuration.getServerId() == server.getId()).findFirst()
                                                .ifPresent(configuration -> {
                                                    configuration.setMinPasswordRole(role.getId());
                                                    configuration.save();
                                                });
                                        interaction.createImmediateResponder().setContent(role.getName() + " is now the minimum required role for viewing stockpile passwords").setFlags(MessageFlag.EPHEMERAL).respond();
                                    }
                                }
                            });
                        })).build()
        );
    }
}
